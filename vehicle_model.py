#makes get requests, cleans data, and posts to the econyoom database
import requests
import json
import xmltodict
from sqlalchemy import Column, Integer, Boolean, String, Date, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from tables import Vehicles, getURI

# clears faildReads.txt
# f = open('failedReads.txt', 'w')
# f.write('')
# f.close()

### Vehicles
renames = {
    'A8L': 'A8 L',
    'RS7': 'RS 7',
    'X5 sDrive40i 2WD': 'X5 sDrive40i'
}

print('Getting data from NREL')
# get all vehicle data from nrel
nrel_vehicle = requests.get("https://developer.nrel.gov/api/vehicles/v1/vehicles.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9")

# get the model, model year, and manufacturer for each vehicle
nrel_vehicle = nrel_vehicle.json()['result']
# econyoom_vehicle_list = []
econyoom_vehicle_dict = {}
for d in nrel_vehicle:

    vehicle = {}
    if d["id"] in econyoom_vehicle_dict:
        continue
    vehicle['id'] = d["id"] 
    vehicle['name'] = d["model"] 
    #not all instances have these fields:
    vehicle['model_year'] = d["model_year"] if "category_name" in d else -1
    vehicle['engine_size'] = d['engine_size'] if "engine_size" in d else 'N/A'
    vehicle['car_type'] = d["category_name"] if "category_name" in d else 'N/A'
    vehicle['manufacturer'] = d["manufacturer_name"] if "manufacturer_name" in d else 'N/A'
    vehicle['fuel_type'] = d["fuel_name"] if "fuel_name" in d else 'N/A'
    vehicle['fuel_id'] = d["fuel_id"] if "fuel_id" in d else -1
    # econyoom_vehicle_list.append(vehicle)
    econyoom_vehicle_dict[d["id"]] = vehicle

print('Data got from NREL')
print('')
# for each vehicle we have, pull the rest of the data if it's available with the make, model, and year we have available so far
for d in econyoom_vehicle_dict:
    c = econyoom_vehicle_dict[d]
    v_year = c['model_year']
    v_make = c['manufacturer']
    if c['name'] in renames:
        c['name'] = renames[c['name']]
    v_model = c['name'].replace(' ', '%20')

    #get fueleconomy's API id for the vehicle
    fueleconomy_vehicle = requests.get(f'https://www.fueleconomy.gov/ws/rest/vehicle/menu/options?year={v_year}&make={v_make}&model={v_model}')
    fueleconomy_vehicle = xmltodict.parse(fueleconomy_vehicle.content)
    print('fueleconomy', v_make, c['name'], v_year)
    print(f'https://www.fueleconomy.gov/ws/rest/vehicle/menu/options?year={v_year}&make={v_make}&model={v_model}')
    try:
        fueleconomy_vehicle = fueleconomy_vehicle['menuItems']['menuItem']['value']
    except KeyError:
        fueleconomy_vehicle = fueleconomy_vehicle['menuItems']['menuItem'][0]['value']
    except TypeError:
        try:
            fueleconomy_vehicle = fueleconomy_vehicle['menuItems']['menuItem'][0]['value']
        #model name mismatch between APIs, write out to failedReads.txt and skip
        except TypeError:
            # file = open('failedReads.txt', 'a')
            # file.write(f"fueleconomy\t{v_make}\t{c['name']}\t{v_year}\n")
            # file.close()
            continue
    
    #get fueleconomy's vehicle data from API id
    print(f'https://fueleconomy.gov/ws/rest/vehicle/{fueleconomy_vehicle}')
    fueleconomy_vehicle = requests.get(f'https://fueleconomy.gov/ws/rest/vehicle/{fueleconomy_vehicle}')
    fueleconomy_vehicle = xmltodict.parse(fueleconomy_vehicle.content)
    fueleconomy_vehicle = fueleconomy_vehicle['vehicle']

    #load to EcoNyoom data. Documentation: https://www.fueleconomy.gov/feg/ws/index.shtml
    c['annual_cost'] = fueleconomy_vehicle['fuelCost08']
    c['mpg'] = fueleconomy_vehicle['cityCD']# + fueleconomy_vehicle['highwaympgCD']
    c['range_mi'] = fueleconomy_vehicle['rangeCityA']
    c['drive_type'] = fueleconomy_vehicle['drive']
    c['avg_savings'] = fueleconomy_vehicle['youSaveSpend']
    c['emissions'] = fueleconomy_vehicle['co2']

    try:
        fueleconomy_vehicle = fueleconomy_vehicle['emissionsList']['emissionsInfo']
        c['epa_fuel_economy_score'] = fueleconomy_vehicle['score']
    except TypeError:
        c['epa_fuel_economy_score'] = 'N/A'

#all data collected, write econyoom_vehicle_list to file
json = json.dumps(econyoom_vehicle_dict)
f = open("vehicles.json","w")
f.write(json)
f.close()


engine = create_engine(getURI())
Session = sessionmaker(bind=engine)

s = Session()
s.query(Vehicles).delete()
s.commit()

for d in econyoom_vehicle_dict:
    # print(d)
    c = econyoom_vehicle_dict[d]
    vehicle = Vehicles(
        id = c["id"],
        name = c["name"],
        car_type = c["car_type"],
        manufacturer = c["manufacturer"],
        fuel_type = c["fuel_type"],
        fuel_id = c["fuel_id"],
        annual_cost = c["annual_cost"],
        mpg = c["mpg"],
        model_year = c["model_year"],
        emissions = c["emissions"],
        range_mi = c["range_mi"],
        epa_fuel_economy_score = c["epa_fuel_economy_score"] if c['epa_fuel_economy_score'] != 'N/A' else -1,
        engine_size = c["engine_size"],
        drive_type = c["drive_type"],
        avg_savings = c["avg_savings"],
    )
    s.add(vehicle)
    s.commit()

s.close()
