#pulls data from vehicles.json and pushes to db
import requests
import json
import xmltodict
from sqlalchemy import Column, Integer, Boolean, String, Date, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from tables import Vehicles, getURI

#reading cached data from vehicles.json
f = open('vehicles.json', 'r')
econyoom_vehicle_dict = json.load(f)
print(len(econyoom_vehicle_dict))

# assert 0

#loading into db
engine = create_engine(getURI())
Session = sessionmaker(bind=engine)

s = Session()
count = 0

for c in econyoom_vehicle_dict:
    count += 1
    d = econyoom_vehicle_dict[c]
    print(count)
    vehicle = Vehicles(
        id = d["id"],
        name = d["name"],
        car_type = d["car_type"],
        manufacturer = d["manufacturer"],
        fuel_type = d["fuel_type"],
        fuel_id = -1 if d["fuel_id"] == 'N/A' else d["fuel_id"], ####
        annual_cost = d["annual_cost"] if "annual_cost" in d else -1,
        mpg = d["mpg"] if "mpg" in d else -1,
        model_year = -1 if d["model_year"] == 'N/A' else d["model_year"],
        emissions = d["emissions"] if "emissions" in d else -1,
        range_mi = d["range_mi"] if "range_mi" in d else -1,
        epa_fuel_economy_score = d["epa_fuel_economy_score"] if 'epa_fuel_economy_score' in d and d['epa_fuel_economy_score'] != 'N/A' else -1,
        engine_size = d["engine_size"],
        charging = d["charging"],
        drive_type = d["drive_type"] if "drive_type" in d else "N/A", 
        avg_savings = d["avg_savings"] if "avg_savings" in d else -1,
    )
    s.add(vehicle)
    s.commit()

s.close()
