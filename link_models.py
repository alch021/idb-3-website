import json
from sqlalchemy.orm import sessionmaker, load_only
from sqlalchemy import *
from tables import *

vehicle_to_es = {
    'Electric': ['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Plug-in Hybrid Electric': ['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Plug-in Hybrid Electric (Non-Blended)':['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Hybrid Electric': ['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'E85/Electric':['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Hydraulic/Electric':['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Methane/Electric':['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Propane/Electric':['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Hybrid - Diesel Electric':['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Hybrid - CNG':['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Hybrid - Diesel':['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Hybrid - Propane':['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Hybrid - LNG':['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Hydraulic Hybrid':['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'Methanol':['Natural Gas'],
    'CNG':['Natural Gas'],
    'CNG - Compressed Natural Gas':['Natural Gas'],
    'CNG - Bi-fuel':['Natural Gas'],
    'LNG - Liquified Natural Gas':['Natural Gas'],
    'Propane': ['Natural Gas'],
    'Propane - Bi-fuel': ['Natural Gas'],
    'Ethanol':['Biomass'],
    'Ethanol (E85)':['Biomass'],
    'Biodiesel (B100)':['Biomass'],
    'Biodiesel (B20)':['Biomass'],
    'Biomethane':['Biomass'],
    'Hydrogen Fuel Cell':['Hydroelectric'],
    'Hydrogen':['Hydroelectric'],
    'Natural Gas':['Natural Gas'],
    'N/A':[],
    'Other': [],
}

inf_to_es = {
    'BD':['Biomass'],
    'CNG':['Natual Gas'],
    'E85':['Biomass'],
    'ELEC':['Wood and wood derived fuels', 'Wind', 'Solar', 'Petroleum Coke', 'Nuclear', 'Natural Gas', 'Liquid Petroleum', 'Hydroelectric', 'Geothermal', 'Coal', 'Biomass'],
    'HY':['Hydroelectric'],
    'LNG':['Natural Gas'],
    'LPG':['Natural Gas'],
}

fuel_types_for_stations = {
    'BD':[],
    'CNG':[],
    'E85':[],
    'ELEC':[],
    'HY':[],
    'LNG':[],
    'LPG':[],
}


#main script
engine = create_engine(getURI())
Session = sessionmaker(bind=engine)

s = Session()

# get all stations of a fuel_type
#for source in fuel_types_for_stations:
#    fuel_types_for_stations[source] = [r.id for r in s.query(Infrastructure).filter(Infrastructure.fuel_type == source).distinct()]
    
# load into intermediary dictionary
vehicle_to_inf = {
    'Electric': fuel_types_for_stations['ELEC'],
    'Plug-in Hybrid Electric': fuel_types_for_stations['ELEC'],
    'Plug-in Hybrid Electric (Non-Blended)':fuel_types_for_stations['ELEC'],
    'Hybrid Electric': fuel_types_for_stations['ELEC'],
    'E85/Electric':fuel_types_for_stations['E85'] + fuel_types_for_stations['ELEC'],
    'Hydraulic/Electric':fuel_types_for_stations['ELEC'],
    'Methane/Electric':fuel_types_for_stations['ELEC'],
    'Propane/Electric':fuel_types_for_stations['ELEC'],
    'Hybrid - Diesel Electric':fuel_types_for_stations['ELEC'],
    'Hybrid - CNG':fuel_types_for_stations['ELEC'],
    'Hybrid - Diesel':fuel_types_for_stations['ELEC'],
    'Hybrid - Propane':fuel_types_for_stations['ELEC'],
    'Hybrid - LNG':fuel_types_for_stations['ELEC'],
    'Hydraulic Hybrid':fuel_types_for_stations['ELEC'],
    'Methanol':fuel_types_for_stations['CNG'],
    'CNG':fuel_types_for_stations['CNG'],
    'CNG - Compressed Natural Gas':fuel_types_for_stations['CNG'],
    'CNG - Bi-fuel':fuel_types_for_stations['CNG'],
    'LNG - Liquified Natural Gas':fuel_types_for_stations['LNG'],
    'Propane': fuel_types_for_stations['LPG'],
    'Propane - Bi-fuel': fuel_types_for_stations['LPG'],
    'Ethanol':fuel_types_for_stations['E85'],
    'Ethanol (E85)':fuel_types_for_stations['E85'],
    'Biodiesel (B100)':fuel_types_for_stations['BD'],
    'Biodiesel (B20)':fuel_types_for_stations['BD'],
    'Biomethane':fuel_types_for_stations['BD'],
    'Hydrogen Fuel Cell':fuel_types_for_stations['HY'],
    'Hydrogen':fuel_types_for_stations['HY'],
    'Natural Gas':fuel_types_for_stations['CNG'],
    'N/A':[],
    'Other': [],
}


for key_fuel_type in vehicle_to_inf:
    s.add(EnergySourceInfrastructure(fuel_type = key_fuel_type, fuel_stations = vehicle_to_inf[key_fuel_type]))
    s.commit()

###### Vehicles

# DO NOT UNCOMMENT
# populate vehicles' fuel_station field
# for fuel_type in vehicle_to_inf:
#     s.query(Vehicles).filter(Vehicles.fuel_type == fuel_type).update({Vehicles.fuel_stations: vehicle_to_inf[fuel_type]}, synchronize_session = False)
    
# populating vehicle's energy_source field
# for fuel_type in vehicle_to_es:
#     s.query(Vehicles).filter(Vehicles.fuel_type == fuel_type).update({Vehicles.energy_source: vehicle_to_es[fuel_type]}, synchronize_session = False)

###### Infrastructure

# populate inf's energy_source field
# for fuel_type in inf_to_es:
#    s.query(Infrastructure).filter(Infrastructure.fuel_type == fuel_type).update({Infrastructure.energy_source: inf_to_es[fuel_type]}, synchronize_session = False)

# populate inf's supported vehicles field
# get all vehicles of a fuel type
fuel_types_for_vehicles = {
    'Electric':[],
    'Plug-in Hybrid Electric':[],
    'Plug-in Hybrid Electric (Non-Blended)':[],
    'Hybrid Electric':[],
    'E85/Electric':[],
    'Hydraulic/Electric':[],
    'Methane/Electric':[],
    'Propane/Electric':[],
    'Hybrid - Diesel Electric':[],
    'Hybrid - CNG':[],
    'Hybrid - Diesel':[],
    'Hybrid - Propane':[],
    'Hybrid - LNG':[],
    'Hydraulic Hybrid':[],
    'Methanol':[],
    'CNG':[],
    'CNG - Compressed Natural Gas':[],
    'CNG - Bi-fuel':[],
    'LNG - Liquified Natural Gas':[],
    'Propane':[],
    'Propane - Bi-fuel':[],
    'Ethanol':[],
    'Ethanol (E85)':[],
    'Biodiesel (B100)':[],
    'Biodiesel (B20)':[],
    'Biomethane':[],
    'Hydrogen Fuel Cell':[],
    'Hydrogen':[],
    'Natural Gas':[],
    'N/A':[],
    'Other':[],
}
#for source in fuel_types_for_vehicles:
#    fuel_types_for_vehicles[source] = [r.id for r in s.query(Vehicles).filter(Vehicles.fuel_type == source).distinct()]

# dict of mappings from inf fuel type to vehicle fuel type
# 'ELEC':fuel_types_for_stations['ELEC'],
inf_to_vehicle = {
    'BD':[],
    'CNG':[],
    'E85':[],
    'ELEC':[],
    'HY':[],
    'LNG':[],
    'LPG':[],
}
for key in fuel_types_for_vehicles:
    if 'Electric' in key or 'Hybrid' in key:
        inf_to_vehicle['ELEC'] += fuel_types_for_vehicles[key]
    elif key == 'Methanol' or key == 'CNG' or key == 'Natural Gas':
        inf_to_vehicle['CNG'] += fuel_types_for_vehicles[key]
    elif key == 'Liquified Natural Gas':
        inf_to_vehicle['LNG'] += fuel_types_for_vehicles[key]
    elif 'Propane' in key:
        inf_to_vehicle['LPG'] += fuel_types_for_vehicles[key]
    elif 'Ethanol' in key:
        inf_to_vehicle['E85'] += fuel_types_for_vehicles[key]
    elif 'Bio' in key:
        inf_to_vehicle['BD'] += fuel_types_for_vehicles[key]        
    elif 'Hydrogen' in key:
        inf_to_vehicle['HY'] += fuel_types_for_vehicles[key]
    else:
        continue
    if 'E85' in key:
        inf_to_vehicle['E85'] += fuel_types_for_vehicles[key]

for key_fuel_type in inf_to_vehicle:
    s.add(InfrastructureVehicles(fuel_type = key_fuel_type, serviceable_vehicles = inf_to_vehicle[key_fuel_type]))
    s.commit()

# DO NOT UNCOMMENT
# populates infrastructures' serviceable_vehicles field
# for fuel_type in inf_to_vehicle:
#     s.query(Infrastructure).filter(Infrastructure.fuel_type == fuel_type).update({Infrastructure.serviceable_vehicles: inf_to_vehicle[fuel_type]}, synchronize_session = False)


### Energy Sources

# Cannibalized mappings of all relevant infrastructure and vehicles
es_to_inf = {
    'Wood and wood derived fuels': fuel_types_for_stations['ELEC'], 
    'Wind': fuel_types_for_stations['ELEC'], 
    'Solar': fuel_types_for_stations['ELEC'], 
    'Petroleum Coke': fuel_types_for_stations['ELEC'], 
    'Nuclear': fuel_types_for_stations['ELEC'], 
    'Natural Gas': fuel_types_for_stations['ELEC'] + fuel_types_for_stations['CNG'] + fuel_types_for_stations['LNG'] + fuel_types_for_stations['LPG'], 
    'Liquid Petroleum': fuel_types_for_stations['ELEC'], 
    'Hydroelectric': fuel_types_for_stations['ELEC'] + fuel_types_for_stations['HY'], 
    'Geothermal': fuel_types_for_stations['ELEC'], 
    'Coal': fuel_types_for_stations['ELEC'], 
    'Biomass': fuel_types_for_stations['ELEC'] + fuel_types_for_stations['BD'] + fuel_types_for_stations['E85'],
}

es_to_vehicle = {
    'Wood and wood derived fuels': inf_to_vehicle['ELEC'], 
    'Wind': inf_to_vehicle['ELEC'], 
    'Solar': inf_to_vehicle['ELEC'], 
    'Petroleum Coke': inf_to_vehicle['ELEC'], 
    'Nuclear': inf_to_vehicle['ELEC'], 
    'Natural Gas': inf_to_vehicle['ELEC'] + inf_to_vehicle['CNG'] + inf_to_vehicle['LNG'] + inf_to_vehicle['LPG'], 
    'Liquid Petroleum': inf_to_vehicle['ELEC'], 
    'Hydroelectric': inf_to_vehicle['ELEC'] + inf_to_vehicle['HY'], 
    'Geothermal': inf_to_vehicle['ELEC'], 
    'Coal': inf_to_vehicle['ELEC'], 
    'Biomass': inf_to_vehicle['ELEC'] + inf_to_vehicle['BD'] + inf_to_vehicle['E85'],
}

# populates energy source to vehicle and supported infrastructure
# for name in es_to_inf:
#     s.query(EnergySource).filter(EnergySource.name == name).update({EnergySource.supported_vehicles: es_to_vehicle[name], EnergySource.supported_charging_stations: es_to_inf[name]}, synchronize_session = False)



# print(fuel_types_for_stations)


'''#s.query(Vehicles).all().update({Vehicles.energy_source: vehicle_to_es[Vehicles.fuel_type]}, synchronize_session = False)
# s.execute(update(Vehicles, values={Vehicles.energy_source: vehicle_to_es[Vehicles.fuel_type]}))

# s.query(Vehicles).all().update({Vehicles.fuel_stations: [Vehicles.fuel_type]}, synchonize_session = False)
# s.query(Infrastructure).all().update({Infrastructure.energy_source: inf_to_es[Infrastructure.fuel_type]}, synchonize_session = False)
'''
s.commit()
s.close()