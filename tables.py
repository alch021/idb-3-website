from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, Boolean, String, Date, Float, ARRAY
from sqlalchemy import create_engine


'''
TABLES
'''
Base = declarative_base()

class Vehicles(Base):
    '''
    Vehicle Name
    Vehicle Type
    Manufacturer
    Fuel Type
    Annual Fuel Cost
    Combined MPG (or equivalent)
    Model Year
    CO2 Emissions
    Range
    EPA Fuel economy Score
    Engine Size
    Compatible Charging Stations
    Drive Type
    How much you save compared
    to an average car. (positive is savings)
    which energy sources support this vehicle
    the fuel stations which support this vehicle
    the url of an image of the vehicle
    '''
    __tablename__ = 'vehicles'
    id = Column(Integer, primary_key=True)
    name = Column(String)
    car_type = Column(String)
    manufacturer = Column(String)
    fuel_type = Column(String)
    fuel_id = Column(Integer)
    annual_cost = Column(Integer)
    mpg = Column(Float)
    model_year = Column(Integer)
    emissions = Column(Float)
    range_mi = Column(Float)
    epa_fuel_economy_score = Column(Float)
    engine_size = Column(String)
    drive_type = Column(String)
    avg_savings = Column(Integer)
    energy_source = Column(ARRAY(String))
    vehicle_image_url = Column(String)

    
    def __repr__(self):
        return "<Vehicle(id='{}', name='{}', car type='{}', manufacturer='{}', fuel type='{}', fuel id='{}', annual cost='{}', mpg='{}', model year='{}', emissions='{}', range in miles='{}', epa fuel economy score='{}', engine size='{}', drive type='{}',  avg_savings='{}', energy_source='{}', vehicle_image_url = '{}')>"\
                .format(self.id, self.name, self.car_type, self.manufacturer, self.fuel_type, self.fuel_id, self.annual_cost, self.mpg, self.model_year, self.emissions, self.range_mi, self.epa_fuel_economy_score, self.engine_size, self.drive_type, self.avg_savings, self.energy_source, self.vehicle_image_url)

    def get_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'car_type': self.car_type,
            'manufacturer': self.manufacturer,
            'fuel_type': self.fuel_type,
            'fuel_id': self.fuel_id,
            'annual_cost': self.annual_cost,
            'mpg': self.mpg,
            'model_year': self.model_year,
            'emissions': self.emissions,
            'range_mi': self.range_mi,
            'epa_fuel_economy_score': self.epa_fuel_economy_score,
            'engine_size': self.engine_size,
            'drive_type': self.drive_type,
            'avg_savings': self.avg_savings,
            'energy_source': self.energy_source,
            'vehicle_image_url': self.vehicle_image_url,
        }

class Infrastructure(Base):
    '''
    Station Name
    Access Days/time
    Public vs. Private Access
    Fuel Type
    Facility Type
    Facility Address
    Cards Accepted
    EV Network
    EV Charging level
    EV Connector Type
    State
    Zip Code
    Country
    Station Phone number
    Status (Currently Opened/Planned)
    Where the renewable comes from
    Servicable Vehicle Models
    '''

    __tablename__ = 'infrastructure'
    id = Column(Integer, primary_key=True)
    station_name = Column(String)
    access = Column(String)
    public = Column(String)
    fuel_type = Column(String)
    facility_type = Column(String)
    address = Column(String)
    cards_accepted = Column(String)
    ev_network = Column(String)
    ev_charging_level = Column(String)
    ev_connector_type = Column(String)
    state = Column(String)
    zip_code = Column(String)
    country = Column(String)
    phone_number = Column(String)
    status = Column(String)
    energy_source = Column(ARRAY(String))


    def __repr__(self):
        return "<Infrastructure(id='{}', station name='{}', access='{}', public='{}', fuel type='{}', facility type='{}', address='{}', cards accepted='{}', ev network='{}', ev charging level='{}', ev connector type='{}', state='{}', zip code='{}', country='{}', phone number='{}', status='{}', energy_source='{}')>"\
            .format(self.id, self.station_name, self.access, self.public, self.fuel_type, self.facility_type, self.address, self.cards_accepted, self.ev_network, self.ev_charging_level, self.ev_connector_type, self.state, self.zip_code, self.country, self.phone_number, self.status, self.energy_source)
            
    def get_dict(self):
        return {
            'id': self.id,
            'station_name': self.station_name,
            'access': self.access,
            'public': self.public,
            'fuel_type': self.fuel_type,
            'facility_type': self.facility_type,
            'address': self.address,
            'cards_accepted': self.cards_accepted,
            'ev_network': self.ev_network,
            'ev_charging_level': self.ev_charging_level,
            'ev_connector_type': self.ev_connector_type,
            'state': self.state,
            'zip_code': self.zip_code,
            'country': self.country,
            'phone_number': self.phone_number,
            'status': self.status,
            'energy_source': self.energy_source,
        }

class EnergySource(Base):
    '''
    Name of Source
    Vehicles that it can support
    Charging stations that use/sell this energy
    Renewability
    Total Power generated (Monthly - As of April 2020)
    Electric Utility (Annually - 2019)
    Independent producers (Annually - 2019)
    All Commercial (Annually - 2019)
    Emissions 
    Outputted Energy Type
    laws and incentives
    CO2 emissions (by metric ton accross the entire US)
    Description/Definition
    the url of an image of the energy source
    the url of an informational page about each energy source
    '''
    __tablename__ = 'energy sources'
    name = Column(String, primary_key=True)
    supported_vehicles = Column(ARRAY(Integer))
    supported_charging_stations = Column(ARRAY(Integer))
    renewability = Column(Boolean)
    total_power = Column(Integer)
    electric_utility = Column(Integer)
    independent_producers = Column(Integer)
    all_commercial = Column(Integer)
    output_energy_type = Column(String)
    laws_incentives = Column(String)
    co2_emissions = Column(Integer)
    description = Column(String)
    image_url = Column(String)
    information_url = Column(String)

    def __repr__(self):
        return "<Energy Source(name='{}', supported vehicles='{}', supported charging stations='{}', renewability='{}', total power='{}', electric utility='{}', independent producers='{}', all commercial='{}', output energy type='{}', laws and incentives='{}', CO2 Emissions='{}', description='{}', image_url='{}', information_url='{}')>"\
            .format(self.name, self.supported_vehicles, self.supported_charging_stations, self.renewability, self.total_power, self.electric_utility, self.independent_producers, self.all_commercial, self.output_energy_type, self.laws_incentives, self.co2_emissions, self.description, self.image_url, self.information_url)
    def get_dict(self):
        return {
            'name': self.name,
            'supported_vehicles': self.supported_vehicles,
            'supported_charging_stations': self.supported_charging_stations,
            'renewability': self.renewability,
            'total_power': self.total_power,
            'electric_utility': self.electric_utility,
            'independent_producers': self.independent_producers,
            'all_commercial': self.all_commercial,
            'output_energy_type': self.output_energy_type,
            'laws_incentives': self.laws_incentives,
            'co2_emissions': self.co2_emissions,
            'description': self.description,
            'image_url': self.image_url,
            'information_url': self.information_url,
        }
        
    def get_dict_model_page(self):
        return {
            'name': self.name,
            'renewability': self.renewability,
            'total_power': self.total_power,
            'electric_utility': self.electric_utility,
            'independent_producers': self.independent_producers,
            'all_commercial': self.all_commercial,
            'output_energy_type': self.output_energy_type,
            'laws_incentives': self.laws_incentives,
            'co2_emissions': self.co2_emissions,
            'description': self.description,
            'image_url': self.image_url,
            'information_url': self.information_url,
        }

class EnergySourceInfrastructure(Base):
    __tablename__ = 'energy source infrastructure'
    fuel_type = Column(String, primary_key=True)
    fuel_stations = Column(ARRAY(Integer))
    
    def __repr__(self):
        return "<Energy Source Infrastructure(fuel_type='{}', fuel_stations = '{}')>"\
            .format(self.fuel_type, self.fuel_stations)
    def get_dict(self):
        return {
            'fuel_type': self.fuel_type,
            'fuel_stations': self.fuel_stations,
        }


class InfrastructureVehicles(Base):
    __tablename__ = 'infrastructure vehicles'
    fuel_type = Column(String, primary_key=True)
    serviceable_vehicles = Column(ARRAY(Integer))
    
    def __repr__(self):
        return "<Infrastructure Vehicles(fuel_type='{}', serviceable_vehicles = '{}')>"\
            .format(self.fuel_type, self.serviceable_vehicles)
    def get_dict(self):
        return {
            'fuel_type': self.fuel_type,
            'serviceable_vehicles': self.serviceable_vehicles,
        }        
'''
CONNECT DATABASE
'''
def getURI():
    return "postgresql+psycopg2://postgres:test@35.238.209.51:5432/Econyoom"

'''
CREATE TABLES
'''

engine = create_engine(getURI())
Vehicles.__table__.create(bind=engine, checkfirst=True)
EnergySource.__table__.create(bind=engine, checkfirst=True)
Infrastructure.__table__.create(bind=engine, checkfirst=True)
EnergySourceInfrastructure.__table__.create(bind=engine, checkfirst=True)
InfrastructureVehicles.__table__.create(bind=engine, checkfirst=True)
