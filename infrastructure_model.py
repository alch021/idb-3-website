#makes get requests, cleans data, and posts to the econyoom database
import requests
# import tables
import json
from sqlalchemy import Column, Integer, Boolean, String, Date, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from tables import Infrastructure, getURI

### Fueling Stations
fuel_stations_request = requests.get("https://developer.nrel.gov/api/alt-fuel-stations/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9")

nrel_fuel_stations = fuel_stations_request.json()['fuel_stations']
econyoom_fuel_station_list = []
id_gen = 1
for d in nrel_fuel_stations:
    fuel_station = {}

    fuel_station['id'] = id_gen
    id_gen += 1

    fuel_station['station_name'] = d["station_name"] if "station_name" in d else 'N/A'
    fuel_station['access'] = d["access_days_time"] if "access_days_time" in d else 'N/A'
    fuel_station['public'] = d["public"] if "public" in d else 'N/A'
    fuel_station['fuel_type'] = d["fuel_type_code"] if "fuel_type_code" in d else 'N/A'
    fuel_station['facility_type'] = d["facility_type"] if "facility_type" in d else 'N/A'
    fuel_station['address'] = d["street_address"] if "street_address" in d else 'N/A'
    fuel_station['cards_accepted'] = d["cards_accepted"] if "cards_accepted" in d else 'N/A'
    fuel_station['ev_network'] = d["ev_network"] if "ev_network" in d else 'N/A'
    fuel_station['ev_charging_level'] = d["ev_charging_level"] if "ev_charging_level" in d else 'N/A'
    fuel_station['ev_connector_type'] = d["ev_connector_type"] if "ev_connector_type" in d else 'N/A'
    fuel_station['state'] = d["state"] if "state" in d else 'N/A'
    fuel_station['zip_code'] = d["zip"] if "zip" in d else -1
    fuel_station['country'] = d["country"] if "country" in d else 'N/A'
    fuel_station['phone_number'] = d["station_phone"] if "station_phone" in d else 'N/A'
    fuel_station['status'] = d["status_code"] if "status_code" in d else 'N/A'
    #fuel_station['source'] = d["ev_renewable_source"] if "ev_renewable_source" in d else 'N/A'
    #fuel_station['serviceable_vehicle_models'] = d["fuel_type_code"] if "fuel_type_code" in d else 'N/A'

    econyoom_fuel_station_list.append(fuel_station)

engine = create_engine(getURI())
Session = sessionmaker(bind=engine)

s = Session()

s.query(Infrastructure).delete()
s.commit()
for d in econyoom_fuel_station_list:
    print(d)
    infrastructure = Infrastructure(
        id = d["id"],
        station_name = d["station_name"],
        access = d["access"], 
        public = d["public"], 
        fuel_type = d["fuel_type"], 
        facility_type = d["facility_type"], 
        address = d["address"], 
        cards_accepted = d["cards_accepted"], 
        ev_network = d["ev_network"], 
        ev_charging_level = d["ev_charging_level"], 
        ev_connector_type = d["ev_connector_type"], 
        state = d["state"], 
        zip_code = d["zip_code"], 
        country = d["country"], 
        phone_number = d["phone_number"], 
        status = d["status"], 
        #source = d["source"], 
        #serviceable_vehicle_models = d["serviceable_vehicle_models"]
    )
    s.add(infrastructure)
    s.commit()

s.close()