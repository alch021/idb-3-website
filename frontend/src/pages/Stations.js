import React, { forwardRef, useEffect, useState } from 'react';
import axios from 'axios';
import MaterialTable from "material-table";
import { Search, FirstPage, LastPage, ChevronLeft, ChevronRight, Clear, ArrowDownward } from "@material-ui/icons";
import { Link } from "react-router-dom";

function Stations() {


  const [stations, setStations] = useState([]);

  useEffect(() => {
  axios.get(`/api/fuel_stations`)
    .then((r) => { 
      setStations(r.data);
    })
    .catch(error => console.log(error));

  }, []);

  console.log(stations);

  const columns = [
      {
        title: 'Name',
        field: 'station_name',
      },
      {
        title: 'Access',
        field: 'access',
      },
      {
        title: 'Public',
        field: 'public',
      },
      {
        title: 'Fuel Type',
        field: 'fuel_type',
      },
      // {
      //   title: 'Fuel ID',
      //   field: 'fuel_id',
      // },
      {
        title: 'Facility Type',
        field: 'facility_type',
      },
      {
        title: 'Address',
        field: 'address',
      },
      {
        title: 'Cards Accepted',
        field: 'cards_accepted',
      },
      {
        title: 'EV Network',
        field: 'ev_network',
      },
      {
        title: 'EV Charging Level',
        field: 'ev_charging_level',
      },
      {
        title: 'State',
        field: 'state',
      },
      {
        title: 'Zip Code',
        field: 'zip_code',
      },
      {
        title: 'Country',
        field: 'country',
      },
      {
        title: 'Phone Number',
        field: 'phone_number',
      },
      {
        title: 'Status',
        field: 'status',
      },
      // {
      //   title: 'Source',
      //   field: 'source',
      // },
      // {
      //   title: 'Servicable Models',
      //   field: 'serviceable_vehicle_models',
      // },
    ];

    let rows = [];

      for (let v = 0; v < stations.length; v++) {
        let instance = stations[v];
        rows.push({
            station_name: <Link to={'/stations/' + instance['id']}>{instance['station_name']}</Link>,
            access: instance["access"], 
            public: instance["public"], 
            fuel_type: instance["fuel_type"], 
            // fuel_id: instance["fuel_id"], 
            facility_type: instance["facility_type"], 
            address: instance["address"], 
            cards_accepted: instance["cards_accepted"], 
            ev_network: instance["ev_network"], 
            ev_charging_level: instance["ev_charging_level"], 
            ev_connector_type: instance["ev_connector_type"], 
            state: instance["state"], 
            zip_code: instance["zip_code"], 
            country: instance["country"], 
            phone_number: instance["phone_number"], 
            status: instance["status"], 
            // source: instance["source"], 
            // serviceable_vehicle_models: instance["serviceable_vehicle_models"]
        });
      }

    const tableIcons = {
      Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
      FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
      LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
      NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
      ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    };

    return (
        <div>
          <MaterialTable title="Fuel Stations" data={rows} columns={columns} icons={tableIcons} options={{ exportButton: false }} />
        </div>
    )
}

export default Stations;