import React, { forwardRef, useEffect, useState } from 'react';
import axios from 'axios';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import MaterialTable from "material-table";
import { Search, FirstPage, LastPage, ChevronLeft, ChevronRight, Clear, ArrowDownward } from "@material-ui/icons";
import { Link } from "react-router-dom";

// Gridlist from Material ui
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

const tileDataFormat = [
  {
      img: "https://miro.medium.com/max/1002/1*tYLog4b5koGAWfQV859ffQ.png",
      link: "https://developer.nrel.gov/docs/transportation/",
      title: 'NREL',
      info: "Used GET requests to grab data from api.",
      cols: 2,
  },
];

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    flexWrap: 'nowrap',
    // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
    transform: 'translateZ(0)',
  },
  title: {
    color: theme.palette.primary.light,
  },
  titleBar: {
    background:
      'linear-gradient(to top, rgba(0,0,0,0.7) 0%, rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)',
  },
}));

function StationInstance() {

  const classes = useStyles();

  let pages = window.location.pathname.split('/');
  let id = pages[2];

  const [data, setData] = useState([]);
  const [tileData, setTileData] = useState([]);
  const [vehicleRows, setVehicleRows] = useState([]);
  
  useEffect(() => {
    axios.get(`/api/fuel_stations/id=${id}`)
    .then((r) => {
      setData(r.data);

      let array = r.data['energy_source'];
     
      if (array !== undefined){
        for (let i = 0; i < array.length; i++) {
          let info = [];
          axios.get(`/api/energy_sources/name=${array[i]}`)
              .then((r) => {
                info.push(
                  {
                    img: r.data['image_url'],
                    title: array[i],
                    info: "",
                    cols: 2,
                  },
        
                )
                setTileData((prevState) => prevState.concat(info));
              });
          
        }
      }

      if (r.data['serviceable_vehicles'] !== undefined) {
        for (let v = 0; v < 5 && r.data['serviceable_vehicles'].length; v++) {
          let vehicle_id = r.data['serviceable_vehicles'][v];
          let rows = [];
          axios.get(`/api/vehicles/id=${vehicle_id}`)
            .then((r) => {
              rows.push({
                vehicle_name: <Link to={'/vehicles/' + vehicle_id}>{r.data['name']}</Link>,
              });
              setVehicleRows((prevState) => prevState.concat(rows));
            });
        }
      }

    });

    
      
    
  }, []);

  console.log(data);

  const vehicleColumns = [
    {
      title: 'Name',
      field: 'vehicle_name',
    },
  ];
  
  

  const tableIcons = {
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  };

  const checkNull = (value) => {
      if (value === null) {
          return "N/A";
      }
      return value;
  }

  const formatAddress = (data) => {
    let address = data['address'];
    if(address !== undefined){
        address = address.replaceAll(" ", "+");
        address = address.concat(",");
        address = address.concat(data['state']);
        address = address.concat("+");
        address = address.concat(data['zip_code']);
        address = address.concat('+');
        address = address.concat(data['country']);
        return address;
    }
  }

  


  return(
    <div>
      <Container>
        <Row>
            <Col>
            <iframe
                width="600"
                height="450"
                frameborder="0" style={{border:"0"}}
                src={"https://www.google.com/maps/embed/v1/place?key=AIzaSyDdWiM3yF5ZagF745-dELROOc0Vs7LiK2k&q=" + formatAddress(data)} 
                allowFullScreen>
            </iframe>
            </Col>
            <Col>
              <h2>{data['station_name']}</h2>
              <ul>
              <li>Access: {data['access']}</li>
              <li>Cards Accepted: {checkNull(data['cards_accepted'])}</li>
              <li>EV Charging Level: {data['ev_charging_level']}</li>
              <li>EV Connector Type: {data['ev_connector_type']}</li>
              <li>EV Network: {checkNull(data['ev_network'])}</li>
              <li>Facility Type: {checkNull(data['facility_type'])}</li>
              <li>Fuel Type: {data['fuel_type']}</li>
              <li>Phone #: {data['phone_number']}</li>
              <li>Public: {data['public']}</li>
              <li>Station Name:	{data['station_name']}</li>
              <li>Status:	{data['status']}</li>
              <li>Address: {data['address']}</li>
              <li>State:	{data['state']}</li>
              <li>Zip Code: {data['zip_code']}</li>
              <li>Country: {data['country']}</li>
              {/* <li>Energy Sources: <ul>{linkArray(data)}</ul></li> */}
              </ul>
            </Col>
        </Row>
        <br />
        <div className={classes.root}>
          <GridList className={classes.gridList} cols={2.5}>
            {tileData.map((tile) => (
              
              <GridListTile key={tile.img}>
                <img src={tile.img} alt={tile.title} />
                <Link to={"/energy/" + tile.title}>
                <GridListTileBar
                  title={tile.title}
                  classes={{
                    root: classes.titleBar,
                    title: classes.title,
                  }}
                />
                </Link>
              </GridListTile>
              
            ))}
          </GridList>
        </div>
        <br />
        <MaterialTable title="Some Servicable Vehicles" data={vehicleRows} columns={vehicleColumns} icons={tableIcons} options={{ exportButton: false }} />
      </Container>
    </div>
  )
  }
  
  export default StationInstance;
  