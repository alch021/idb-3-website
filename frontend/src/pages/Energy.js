import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from "react-router-dom";
import Pagination from '@material-ui/lab/Pagination'
import Grid from '@material-ui/core/Grid'
import tempImg from '../images/boltEV.jpg';

import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';

const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
  }));


function Energy() {
    const [energy, setEnergy] = useState([]);
    const [pageNum, setPageNum] = useState(1);

    const [topLeft, setTopLeft] = useState([]);
    const [btmLeft, setBtmLeft] = useState([]);
    const [topRight, setTopRight] = useState([]);
    const [btmRight, setBtmRight] = useState([]);

    const classes = useStyles();


    useEffect(() => {
        axios.get(`/api/energy_sources`)
        .then((r) => {
            let dataArray = r.data;
            setEnergy(dataArray);
            updateData(dataArray, 1)
        })
        .catch(error => console.log(error));
    }, []);

    console.log(energy);

    const handleChange = (e, value) => {
        setPageNum(value);
        updateData(energy, value);
    }

    const updateData = (dataArray, value) => {
        let idx = value - 1;
        setTopLeft(dataArray[idx * 4]);
        setBtmLeft(dataArray[idx * 4 + 1]);
        setTopRight(dataArray[idx * 4 + 2]);
        if (value !== 3) {
            setBtmRight(dataArray[idx * 4 + 3]);
        } else {
            setBtmRight([]);
        }
    }

    const displayData = (arr) => {
        if(arr.length !== 0){
            let grid = [];
            grid.push(
                <Grid container item direction='row' justify='center' alignItems='stretch' xs>
                    <Card className={classes.root}>
                        <CardHeader
                            title={arr['name']} titleTypographyProps={{variant:'h5'}}
                        />
                        <Link to={'/energy/' + arr['name']} >
                        <CardMedia
                            className={classes.media}
                            image={arr['image_url']}
                            title={arr['name'] + " img"}
                        />
                        </Link>
                        <CardContent>
                            <Typography variant="subheading" color="textSecondary" component="p">

                            <br/>Renewability: {arr["renewability"]}
                            <br/>Total Power: {arr["total_power"]}k Megawatthours
                            <br/>Electric Utility: {arr["electric_utility"]}k Megawatthours
                            <br/>Independent Producers: {arr["independent_producers"]}k Megawatthours 
                            <br/>Output Energy Type: {arr["output_energy_type"]} 
                            <br/>CO2 Emissions: {arr["co2_emissions"]}
                            </Typography>
                        </CardContent>
                    </Card>
                </Grid>     
            )
            return grid;
        }
    }


    return (
        <div>
            <Grid container direction='columns' item xs={12} spacing={3} >
                {displayData(topLeft)}
                {displayData(btmLeft)}
            </Grid>
            <Grid container direction='columns' item xs={12} spacing={3}>
                {displayData(topRight)}
                {displayData(btmRight)}
            </Grid>
        <br/>
        <Grid container justify = "center">
            <Pagination count={3} onChange={handleChange}/>
        </Grid>
        
        </div>
    )
}



export default Energy;

