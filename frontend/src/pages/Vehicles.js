import React, { forwardRef, useEffect, useState } from 'react';
import axios from 'axios';
import MaterialTable from "material-table";
import { Search, FirstPage, LastPage, ChevronLeft, ChevronRight, Clear, ArrowDownward } from "@material-ui/icons";
import { Link } from "react-router-dom";

function Vehicles() {


  const [vehicles, setVehicles] = useState([]);

  useEffect(() => {
  axios.get(`/api/vehicles?limit=10`)
    .then((r) => { 
      setVehicles(r.data);
      console.log(r.data);
    })
    .catch(error => console.log(error));

  }, []);

  console.log(vehicles);

  const columns = [
      {
        title: 'Name',
        field: 'name',
      },
      {
        title: 'Car Type',
        field: 'car_type',
      },
      {
        title: 'Manufacturer',
        field: 'manufacturer',
      },
      {
        title: 'Fuel Type',
        field: 'fuel_type',
      },
      {
        title: 'Annual Cost',
        field: 'annual_cost',
      },
      {
        title: 'MPG',
        field: 'mpg',
      },
      {
        title: 'Model Year',
        field: 'model_year',
      },
      {
        title: 'Emissions',
        field: 'emissions',
      },
      {
        title: 'Range Mi',
        field: 'range_mi',
      },
      {
        title: 'EPA Score',
        field: 'epa_fuel_economy_score',
      },
      {
        title: 'Engine Size',
        field: 'engine_size',
      },
      {
        title: 'Drive Type',
        field: 'drive_type',
      },
      {
        title: 'Average Savings',
        field: 'avg_savings',
      },
      // {
      //   title: 'Energy Source',
      //   field: 'energy_source',
      // },
      // {
      //   title: 'Fuel Stations',
      //   field: 'fuel_stations',
      // },
    ];

    let rows = [];

      for (let v = 0; v < vehicles.length; v++) {
        let instance = vehicles[v];
        if (instance['car_type'] !== 'N/A') {
          rows.push({
            name: <Link to={'/vehicles/' + instance['id']} >{instance['name']}</Link>,
            car_type: instance['car_type'],
            manufacturer: instance['manufacturer'],
            fuel_type: instance['fuel_type'],
            annual_cost: instance['annual_cost'],
            mpg: instance['mpg'],
            model_year: instance['model_year'],
            emissions: instance['emissions'],
            range_mi: instance['range_mi'],
            epa_fuel_economy_score: instance['epa_fuel_economy_score'],
            engine_size: instance['engine_size'], 
            // charging: instance['charging'],
            drive_type: instance['drive_type'],
            avg_savings: instance['avg_savings'],
            // energy_source: instance['energy_source'],
            // fuel_stations: instance['fuel_stations'],
          });
        }
      }

    const tableIcons = {
      Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
      FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
      LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
      NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
      PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
      ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
      SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    };

    return (
        <div>
          <MaterialTable title="Vehicles" data={rows} columns={columns} icons={tableIcons} options={{ exportButton: false }} />
        </div>
    )
}

export default Vehicles
