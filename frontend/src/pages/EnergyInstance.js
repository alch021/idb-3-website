import React, { forwardRef, useEffect, useState } from 'react';
import axios from 'axios';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import wikiLogo from '../images/wiki-logo.png'
import eiaLogo from '../images/eia.png'
import MaterialTable from "material-table";
import { Search, FirstPage, LastPage, ChevronLeft, ChevronRight, Clear, ArrowDownward } from "@material-ui/icons";
import { Link } from "react-router-dom";

function EnergyInstance() {
  let pages = window.location.pathname.split('/');
  let name = pages[2];

  const [data, setData] = useState([]);
  const [vehicleRows, setVehicleRows] = useState([]);
  const [stationRows, setStationRows] = useState([]);


  useEffect(() => {
    axios.get(`/api/energy_sources/name=${name}`)
      .then((r) => {
        setData(r.data);

        // Get some supported vehicles for our table
        if (r.data['supported_vehicles'] !== undefined) {
          for (let v = 0; v < 5 && r.data['supported_vehicles'].length; v++) {
            let vehicle_id = r.data['supported_vehicles'][v];
            let rows = [];
            axios.get(`/api/vehicles/id=${vehicle_id}`)
              .then((r) => {
                rows.push({
                  vehicle_name: <Link to={'/vehicles/' + vehicle_id}>{r.data['name']}</Link>,
                });
                setVehicleRows((prevState) => prevState.concat(rows));
              });
          }
        }

        // Get some supported charging stations for our table
        if (r.data['supported_charging_stations'] !== undefined) {
          for (let s = 0; s < 5 && r.data['supported_charging_stations'].length; s++) {
            let station_id = r.data['supported_charging_stations'][s];
            let rows = [];
            axios.get(`/api/fuel_stations/id=${station_id}`)
              .then((r) => {
                rows.push({
                  station_name: <Link to={'/stations/' + station_id}>{r.data['station_name']}</Link>,
                });
                setStationRows((prevState) => prevState.concat(rows));
              });
          }
        }

      });
  }, []);

  const checkTrue = (bool) => {
    if (bool) {
      return 'True';
    }
    return 'False';
  }

  const getLink = (name) => {
    if (name === "Wood and wood derived fuels") {
      return wikiLogo;
    }
    return eiaLogo;
  }

  const vehicleColumns = [
    {
      title: 'Name',
      field: 'vehicle_name',
    },
  ];

  const stationColumns = [
    {
      title: 'Name',
      field: 'station_name',
    },
  ];



  const tableIcons = {
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
  };

  console.log(data)
  return (
    <div>
      <Container>
        <Row>
          <Col>
            <img src={data['image_url']} className="img-fluid " alt={data['name'] + "image"} />
          </Col>
          <Col>
            <h2>{data['name']}</h2>
            <ul>
              <li>Description: {data["description"]}</li>
              {/* <br/>{data["supported_vehicles"]} */}
              {/* <br/>{data["supported_charging_stations"]} */}
              <li>Renewability: {checkTrue(data["renewablility"])}</li>
              <li>Total Power: {data["total_power"]}k Megawatthours</li>
              <li>Electric Utility: {data["electric_utility"]}k Megawatthours</li>
              <li>Independent Producers: {data["independent_producers"]}k Megawatthours </li>
              <li>All Commercial: {data["all_commercial"]}k Megawatthours </li>
              <li>Output Energy Type: {data["output_energy_type"]} </li>
              {/* <br/>Law Incentives: {data["laws_incentives"]} */}
              <li>CO2 Emissions: {data["co2_emissions"]}</li>
            </ul>
            <hr />
            <a href={data['information_url']}>
              <img src={getLink(data['name'])} width="30" height="30" />
            </a>
          </Col>
        </Row>
        <br />
        <MaterialTable title="Some Supported Vehicles" data={vehicleRows} columns={vehicleColumns} icons={tableIcons} options={{ exportButton: false }} />
        <br />
        <MaterialTable title="Some Supported Stations" data={stationRows} columns={stationColumns} icons={tableIcons} options={{ exportButton: false }} />
      </Container>
    </div>
  )
}

export default EnergyInstance;
