import React from 'react';
import MemberInfo from '../components/MemberInfo'
import ApiToolInfo from '../components/ApiToolInfo'

function About() {

    return (
        <div>
            <hr />
            <h1 className="text-center">Motivation</h1>
            <br /><br />
            <p>
                The purpose and motivation of this site is to display information about ecological vehicles, infrastructure in
                places within North America which supports ecological vehicles, as well as the many sources of energy that we use
                to power not only our vehicles but also our everyday lives. We hope that the information, as well as the interlinking
                between it, will help anyone who visits the website become more knowledgeable about the types and usage of energy
                in North America. We also hope that this will be a useful tool in convincing more people of the viability of
                electric cars by calming apprehensions they may have and providing reliably sourced information about the vehicles
                and infrastructure that can support them.
                <br /><br />
                With the combined datasets, we wish to show the varying levels of convenience for owning an ecological vehicle
                by displaying the locations of fueling stations and the energy they support.
                <br /><br />
                We are also planning on creating a RESTful API to provide all the information we will display in an easy to access
                format that other developers can use for free. We hope to make this accessible and sortable in many ways so that
                any customers will have a better time utilizing our data.
            </p>
            <hr />


            <MemberInfo />
            <ApiToolInfo />

        </div>
    )

    
}

export default About
