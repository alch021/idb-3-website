import React, { Component, useState } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './css/bootstrap.min.css';

import Header from './components/Header';
import NavBar from './components/NavBar';
import EcoCarousel from './components/EcoCarousel';
import Home from './components/Home';
import Footer from './components/Footer';

// Import our pages
import About from './pages/About';
import Vehicles from './pages/Vehicles.js';
import Stations from './pages/Stations.js';
import Energy from './pages/Energy.js';
import VehicleInstance from './pages/VehicleInstance.js';
import StationInstance from './pages/StationInstance.js';
import EnergyInstance from './pages/EnergyInstance.js';

function App() {

    return (
      <Router>
      <div className="App container full-height">
          <Header />
          <NavBar />
          <Route exact path="/" render={props => (
          <React.Fragment>
            <EcoCarousel />
            <Home />
          </React.Fragment>
          )}/>
          
          <Route exact path="/about" component={About} />
          <Route exact path="/vehicles" component={Vehicles} />
          <Route exact path="/stations" component={Stations} />
          <Route exact path="/energy" component={Energy} />
          <Route exact path="/vehicles/:id" component={VehicleInstance} />
          <Route exact path="/stations/:id" component={StationInstance} />
          <Route exact path="/energy/:name" component={EnergyInstance} />
          <Footer />
      </div>
      </Router>
    );
}

export default App;
