import React from 'react';
import '../css/style.css';
import { Link } from "react-router-dom";

//images 
import homeCar from '../images/home_car_2.jpg';
import station from '../images/charging_1.jpg';
import energy from '../images/home_energy_3.jpg';

// react-bootstrap to lay things out better
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// Card tools from material ui
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import { red } from '@material-ui/core/colors';
import CardActions from '@material-ui/core/CardActions';
import Button from '@material-ui/core/Button';

const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
  }));

function Home() {
    const classes = useStyles();
    
    return (
        <div>
            <hr />
            <br />
            <h2 className="font-weight-light text-center">Come explore!</h2>
            <br />
            <p className="text-center">Here you can look through our vehicles and see their price as well as their orignating energy sources. 
                You can also find fueling stations in your area and learn about all the different kinds of energies out there!
            </p>
            <br />
            <Container>
                <Row>
                    <Col>
                    <Card className={classes.root}>
                        <CardHeader
                            title={'Vehicles'} titleTypographyProps={{variant:'h4'}}
                        />
                        <Link to={'/vehicles'} >
                            <CardMedia
                                className={classes.media}
                                image={homeCar}
                                title="vehicle"
                            />
                        </Link>
                        <CardActions>
                            <Link to={'/vehicles'} >
                            <Button size="small" color="primary">
                                Explore vehicles!
                            </Button>
                            </Link>
                        </CardActions>
                    </Card>
                    </Col>
                    <Col>
                    <Card className={classes.root}>
                        <CardHeader
                            title={'Fuel Stations'} titleTypographyProps={{variant:'h4'}}
                        />
                        <Link to={'/stations'} >
                            <CardMedia
                                className={classes.media}
                                image={station}
                                title="station"
                            />
                        </Link>
                        <CardActions>
                            <Link to={'/stations'} >
                            <Button size="small" color="primary">
                                Explore stations!
                            </Button>
                            </Link>
                        </CardActions>
                    </Card>
                    </Col>
                    <Col>
                    <Card className={classes.root}>
                        <CardHeader
                            title={'Energy Sources'} titleTypographyProps={{variant:'h4'}}
                        />
                        <Link to={'/energy'} >
                            <CardMedia
                                className={classes.media}
                                image={energy}
                                title="energy"
                            />
                        </Link>
                        <CardActions>
                            <Link to={'/energy'} >
                            <Button size="small" color="primary">
                                Explore energy!
                            </Button>
                            </Link>
                        </CardActions>
                    </Card>
                    </Col>
                </Row>
            </Container>
            <br />
            <br />

        </div>
        

    )
}

export default Home
