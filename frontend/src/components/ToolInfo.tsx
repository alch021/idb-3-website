import React from 'react'

// Gridlist from Material ui
import { makeStyles } from '@material-ui/core/styles';
import GridList from '@material-ui/core/GridList';
import GridListTile from '@material-ui/core/GridListTile';
import GridListTileBar from '@material-ui/core/GridListTileBar';
import IconButton from '@material-ui/core/IconButton';
import InfoIcon from '@material-ui/icons/Info';

// Popover from Material ui
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';

const tileData = [
    {
        img: "https://blog.qualys.com/wp-content/uploads/2020/10/postman.jpg",
        link: "https://www.postman.com/",
        title: 'Postman',
        info: "Used for API testing, design, and documentation",
        cols: 2,
    },
    {
        img: "https://user-images.githubusercontent.com/674621/71187801-14e60a80-2280-11ea-94c9-e56576f76baf.png",
        link: "https://code.visualstudio.com/",
        title: 'VS Code',
        info: "Used for the majority of coding by almost everyone. VS Share was often used to collaborate on things in real time.",
        cols: 2,
    },
    {
        img: "https://www.kindpng.com/picc/m/163-1636552_color-icon-html-css-js-bootstrap-hd-png.png",
        link: "https://getbootstrap.com/",
        title: 'Bootstrap',
        info: "Used to create and stylize our web pages with HTML and CSS.",
        cols: 2,
    },
    {
        img: "https://www.ilovedevops.com/img/avatar-icon.png",
        link: "https://gitlab.com/",
        title: 'GitLab',
        info: "Used to host all the code on and version control.",
        cols: 2,
    },
    {
        img: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAABC1BMVEX/////jET/UgD+WQT/VAH8XQf6Xgr9WwX4Xg3+VgL2XQ/3XQ7yWRPwVhX/hDL8Xgj/5tnpSxztUhjlRB/mRh7uUxfjQCHrThrgOiPfOCT/hzn/SADcMib/kEb/TAD/QgDYKin/+fX/8Or/6eH/1cD/waD/38//z8H/4Nb/uJL/8+3/5tj/kU3/flb/givUAAD/lnbiKADtdlvuQwD/mFr/bjn/zbT9tp3+kGr/vJn/hV7/dEP6gUL/yrv/sIX/wbDfXF3qmpnSFSneQTTZIRnmalzxqJ/gRTL/q33qd2X/pYvpXDf/nmbpOwDwdVHtXi//Zi3+e0n+rZH5fDroWzvvbEHZMjH/imb/m4GWylVYAAALAklEQVR4nO2deVvbuBaHg5c4zkJIWAIx2CRhCWnZmjIwLKVDO0vpbQeYyyXf/5Nc75ZtydKxnTzPvc/58XekvLyS7CjHSqWCwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDCY/6MMJivvVtgZDRb0Noaj0XAOfe19et8/Wc7MydLH/YPyeyYzuX9rGpaTxvHlqMSGVz6dLPeXBNJf7u/PT+XBsWWZpuTGNA3r6b6khkefT193RPg8yP5+Sf0mMjy2fLogpiUfltHyb6e7uw9flgCMOytl9JvIZZLPY7wu3vJnG9BGfLgVR1xa/qV4v4lcW2k+J4ZccDoOvnzd9QJCPCl7pF4YdEBbozEu1PKXr9vb2zkQS7b4zAR0EItY/N0B3A4gxQFti2VeN84YQzRYVvO3/Mf51tZWxPgnQOJSv7yrxl0moD0Xcy83w/PNzc2tEHJ3F3DRWOr/VRphNp8dK+9F49vqxmacETRO35UE+JgxCf1xqudreXS+YWfTh3QRv0PG6b/KARxwxqgr8S5X09/WVleTjCCJ5Sw2M65CW+JFnpZHP2zCJOPiJU4EAG2Jea4YHzpra2lGiMTlMiTOTEWA0LiEtzz+0XEQQ0Z/0Vm0RHsWKgqf0TyGN/2h1+nQGBcscWbfbyt8RlMCtzz+4RJGjMFgBUn8WBRwaEgOHZ/RmECb/rne69EZbxcp8cxUPDoeI3ipGU+73S6d8e8FShxaih8eI5jwZ6vb9Rh7IaM/IRcp0VfoM2ZBWsDPUOPp+vp6nDFadBYncegupEKMUIevrdY6k3FjYRKvTCUeNiNwpRlPW60MRpDE9/kBo1kYg6Qzwpp+bba8MBhBEvdyE6YUshmBV/zDaTNADEXGGBczE0c0hQxG4F3ba6PZjDF2A5EB40IkvtEVUhlhC42jsJlgTAzWRczEkaEoOo/RhwR+erpoNJs8xgVIfJMcvkzGUKQF2t8PFcYgE4zzn4kjS/f4BBjNJqjpi0a9zmecu8RrW6HDqGcyeoMVtolxqNed8BjnPRNHhm4nZMwW+QRq+qlR90NhJCDnLPFY0nVRRpjCO70ehcHoIs5X4oqlBwkYmYMVqrBar/MZHci5SryWdCJKJiNsP/hObzTqYozzlDg2ZFnX45AsRgl2LaxXG06YjOSi88/8JD4rspxgVBTi4qHnVniv12o1QcbWNYAQJtFR6CXGSF489LwK27UAMQPSQ5wOPwlVL+SQeKzIciajXkhhxJg5IRtnlcnyfCRGCvmM0g0EsFJr18LwPE6HlcqcJD7rssxk1OOMFmgP4V6vVqsJRpZHW2GlMjkBSFwSfRsHppwKa2GVniGAlWq7Wk0z0j06CoESRb/aTylMM9pwHiNsi81VmGBkLazamfuSecxEmkKqRxsTqLDdDgD5Hj2FlcpR+RJvZM3+40M6gxSm8FIPEbke1Zn/ovJn4oGpOZEZlDFGCbb/pKltArFGQqYukNNwd7J0iRey5iEKeIQVCzkKvVBFxhC1x/BloJkoIPHQUyjkEaZw4CqMI9bSjC5kQyY2mEuWGCgU8QgrhrIVqhFj9oQkFNr/mlIlHkqaqpHJ8Ci9QQArkkoCZnuUYuVOpUp80jQ1gagxROqwPdIXXVUTjEyPMYXlSryTVC9aQiQlMIUDWfObbrfT8zHBqCUq1iAS+9kSn4K3kUCkQcJm4YseNu0AZkJqL8l/T2kS75TwXaQQk8uOcgUBHEhaRMjwGE5IPVV0WJrEumZ3zYaMEVpDCOGj7jSgahxIlzGlECixz34bd4rbH4mYnJAhpXIGARwoQQukSIZHmVI3+ms5Ep+8LlURjyZMYUgY90i7QMpphfa/CEDInon34V2VGhNJ8whTOJGI16tabKymCamlv6VIrBP9EYxUQpjCmSLHmiAhyfnoUOr071oHkI8YjJnofTylMaZHK1Ch/YnTvXWIU1LXnLbGaAQkkf6oQs0bJ7GbDhahCaq8mDlbdxmI7j/T75GhsIyZGO0wMDwSo1Wf0VpgZRhuGiSXrfS1Q1WZzewXlVglr0p0jyGiBFJ4JkWEbI/ewspUWPya6GzVVmmMqpryqD+mX8/OML77mi2SNQudFJSoujdMVTpkQqQEepQjUugTZnjMUAiciSmJl7J/18vyGDECFY5oW3cMj1o7s6lCEonN9pp/88SakFr6xjgrVxKFMMnoN61kl3QUkXgpNxpxxjRi4FGn3VUxM7Ko+8vpVcddxziNFZCoeZsHdI/VuEdYjd6V82WyIKLErcoBSSSH2qVWrycQmR6BCg1v04ruMTFWq9zm8kocqN4+ZUIk1aNKvzFmJaoHoBOSkHyFuSW+yP4+LGWsJhEz1/NUAoVCHrMXUi85JRIFLg2OR1WGANoKFZ3HGHg0hWrjIBKXA4kvGvmVSNJjfELCFI4txf0eTsCjLNeFmswjcaA1m800I3Wwttk3xrQcS35Zg4BIU7CwCuAwmImPmvuNZD0FSfEIVeh/U6wLMIopBEr81XnFICz25HtsiywGUY5NvwRH53sUVQibiSeORF9h2mM97RE+C5WorEHPYtTFa+N+AUqcxOt102OVoGzzL8lknoPHUaLSDSajuELwTHx0Hu1gMsYnpA6qdR4Tpf6hRwYjQCFU4mDqFViJeKyKLgZebuKl/pkeIQphM3HprLneSjPSF1aYwgMrjkfW/SUhQQphEpf+cYoceYyeRqjC2ENhESht0TGAJxZAHO783SUYMwcrTOGhRSFkMCrQZ91BEm+7XSHGRhU2ki6ogCRjRAlVCJXY63qQHEYd9sQB85mp9MIKVmhLhOws3va8p6o4jA3gQyP+I+BskdHCClcIltjrBYzsRSeHQhFG2yOwQtULTGKnx2dswEbSkxk8EsX3CKtQDQKT2OEzTmFPHLjHoSgCHnMqBEvs8BiBCm/8g88UHiS4vDEKVCKDsZVL4ZA40YbHCKxQjQKTuLbWCSCpC2sDNpLuYwe+ZDPmVVipvAdJXCMYe2mPMIWVq8TpfBmMwApVMjCJq2spRsJj8xXW91Pq/EEmY36F0Jm4ymB0IafA9ZxywCLj4pHnWJsweyCJG6sZjECFA/q5SxRG8Gka8cBmooPIYJwC3wbzZKkUo1ns5EyYxM0NFmPrJ7BjhkPKBTLX+VlEQBK/O0ffrKYgbUKoQvo8pC06BRWCJW7SGdehCmlrKZ2xqEKoxE064w/4YnDFO+LNYzRhdeK0wCRubfqMsQnZ+wDv955/iJ27kJZwmDRM4haNMYfC2H0pm7EEhVCJ21tpxk4Ohd4nfG7KUAiVuB0whhNyLY9Ce5gKSDRBdeLMACVuJxnXvuXrV+JLhNWJs/MRKDHBeJ5zJPElGqAK1YzAJO5uB4zehFzNNQud3PAkmqUdcwyS+O/dOGNehfzlNPfJw+mMQI+bOGdqb4eQ57/l7/cwE9EClR1xsgcpAb99IBi//l6k33vaOfwBIKjGmJsjgMWd1wfvAHgH8M9i/bIRLVD5pkByIe5+/VJ0LRgr1Ls3E3ZsllD2AQN15/nBZTz9XELHZ2mNpnVT0oUwloO++BduO7f/sRFPCywyREbHlkFAmoZ1Ud4iGs+R2K+4eIzfTz+X9hsyo8emZRmGYRqGZUmzYr8UkZnJ0bLgT9X0T/4q9zdOJncvs6u32eXhPIZnLAdHH7k/N7T8/tPegn5VaU6ZjDJ+MerdaPK/TYfBYDAYDAaDwWAwGAwGg8FgMBgMBoPBYDAYDAaDwWAwGAwGg8FgMJhE/gtUmtv+tEjBbAAAAABJRU5ErkJggg==",
        link: "https://www.namecheap.com/",
        title: 'Namecheap',
        info: "Used to get our web domain.",
        cols: 2,
    },
    {
        img: "https://miro.medium.com/max/4000/1*b_al7C5p26tbZG4sy-CWqw.png",
        link: "https://aws.amazon.com/",
        title: 'AWS',
        info: "Used to host the web content on, specifically we are using ‘Amplify’ to host our pages on to connect to our domain.",
        cols: 2,
    },
];

const useStyles = makeStyles((theme) => ({
    gridList: {
        width: 500,
        height: 450,
    },
    icon: {
        color: 'rgba(255, 255, 255, 0.54)',
    },
    popover: {
        pointerEvents: 'none',
    },
    paper: {
        padding: theme.spacing(1),
    },
}));

function ToolInfo() {
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null);
    const [info, setInfo] = React.useState<String | null>(null);

    const handlePopoverOpen = (event: React.MouseEvent<HTMLElement, MouseEvent>, data: String) => {
        setAnchorEl(event.currentTarget);
        setInfo(data);
    };


    const handlePopoverClose = () => {
        setAnchorEl(null);
        setInfo(null);
    };


    const popover = (str: String, data: String) => {
        let array = [];
        array.push(
            <Typography
                aria-owns={open ? 'mouse-over-popover' : undefined}
                aria-haspopup="true"
                onMouseEnter={(e) => handlePopoverOpen(e, data)}
                onMouseLeave={handlePopoverClose}
            >
                {str}
            </Typography>
        )
        return array;
    }

    const open = Boolean(anchorEl);

    return (
        <div>
            <GridList cellHeight={180} className={classes.gridList}>
                    <GridListTile key="Subheader" cols={2} style={{ height: 'auto' }}>
                    </GridListTile>
                    {tileData.map((tile) => (
                        <GridListTile key={tile.img}>
                            <img src={tile.img} alt={tile.title} />
                            <GridListTileBar
                                title={popover(tile.title, tile.info)}
                                actionIcon={
                                    <a href={tile.link}>
                                    <IconButton aria-label={`info about ${tile.title}`} className={classes.icon}>
                                        <InfoIcon />
                                    </IconButton>
                                    </a>
                                }
                            />
                            <Popover
                                id="mouse-over-popover"
                                className={classes.popover}
                                classes={{
                                paper: classes.paper,
                                }}
                                open={open}
                                anchorEl={anchorEl}
                                anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'left',
                                }}
                                transformOrigin={{
                                vertical: 'top',
                                horizontal: 'left',
                                }}
                                onClose={handlePopoverClose}
                                disableRestoreFocus
                            >
                                <Typography>{info}</Typography>
                            </Popover>
                        </GridListTile>
                    ))}
                </GridList>
        </div>
    )
}

export default ToolInfo;
