import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return (
            <footer className="page-footer font-small blue">
            <div className="footer-copyright text-center py-3">Website:
            <a href="/"> Econyoom.me</a>
            </div>
            </footer>
        );
    }
}

export default Footer
