import React, { Component } from 'react';

class Header extends Component {
  render() {
    return (
        <div className="App">    
            <h1 className="page-header">EcoNyoom <small>ecological vehicles</small></h1>
            <p className="lead">For your ecological needs</p>
            {/* <p>A vehicle shopper/browser for customers looking for an ecological-friendly option that provides them
            information about how easy it will be to manage the vehicle as well as pricing.</p> */}
        </div>
    );
    }
}

export default Header;
