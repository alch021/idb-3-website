import React, { useState, useEffect } from 'react';
import axios from 'axios';

// react-bootstrap
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

// images
import kyleImg from '../images/Kyle.png'
import andrewImg from '../images/Andrew.png'
import alexImg from '../images/Alex.png'
import frankeImg from '../images/Franke.png'
import trungImg from '../images/Trung.png'
import alisonImg from '../images/Alison.png'

// Card tools from material ui
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';


const useStyles = makeStyles((theme) => ({
    root: {
      maxWidth: 345,
    },
    media: {
      height: 0,
      paddingTop: '56.25%', // 16:9
    },
    expand: {
      transform: 'rotate(0deg)',
      marginLeft: 'auto',
      transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
      }),
    },
    expandOpen: {
      transform: 'rotate(180deg)',
    },
    avatar: {
      backgroundColor: red[500],
    },
  }));

function MemberInfo() {

    const classes = useStyles();

    const [totalIssues, setTotalIssues] = useState(0);
    const [totalCommits, setTotalCommits] = useState(0);
    const [totalTests, setTotalTests] = useState(0);

    const [kyleIssues, setKyleIssues] = useState(0);
    const [kyleCommits, setKyleCommits] = useState(0);
    const [kyleTests, setKyleTests] = useState(0);

    const [andrewIssues, setAndrewIssues] = useState(0);
    const [andrewCommits, setAndrewCommits] = useState(0);
    const [andrewTests, setAndrewTests] = useState(0);

    const [alexIssues, setAlexIssues] = useState(0);
    const [alexCommits, setAlexCommits] = useState(0);
    const [alexTests, setAlexTests] = useState(0);
    
    const [frankeIssues, setFrankeIssues] = useState(0);
    const [frankeCommits, setFrankeCommits] = useState(0);
    const [frankeTests, setFrankeTests] = useState(0);
    
    const [trungIssues, setTrungIssues] = useState(0);
    const [trungCommits, setTrungCommits] = useState(0);
    const [trungTests, setTrungTests] = useState(0);

    const [alisonIssues, setAlisonIssues] = useState(0);
    const [alisonCommits, setAlisonCommits] = useState(0);
    const [alisonTests, setAlisonTests] = useState(0);

    useEffect(() => {

        // Get Total Issues
        for (let pageNum = 1; pageNum < 5; pageNum++){
            axios.get(`https://gitlab.com/api/v4/projects/21238733/issues?per_page=100&page=${pageNum}`)
                .then(r => {
                    console.log(r.data.length);
                    setTotalIssues((prevState) => prevState += r.data.length);
                })
                .catch(error => console.log(error));
        }
        
        let usernames = [
            'kcolburn38',
            'AndrewElvisDeng',
            'TheNecroreaper',
            'ftang21',
            'EatWorkSleep',
            'alch021'
        ];

        // Get Member Issues
        for (let uname in usernames) {
            axios.get(`https://gitlab.com/api/v4/projects/21238733/issues_statistics?assignee_username=${usernames[uname]}`)
                .then(r => {
                    return r.data;
                })
                .then(data => {
                    if (usernames[uname] === 'kcolburn38') {
                        setKyleIssues(data.statistics.counts.closed);
                    } else if (usernames[uname] === 'AndrewElvisDeng') {
                        setAndrewIssues(data.statistics.counts.closed);
                    } else if (usernames[uname] === 'TheNecroreaper') {
                        setAlexIssues(data.statistics.counts.closed);
                    } else if (usernames[uname] === 'ftang21') {
                        setFrankeIssues(data.statistics.counts.closed);
                    } else if (usernames[uname] === 'EatWorkSleep') {
                        setTrungIssues(data.statistics.counts.closed);
                    } else if (usernames[uname] === 'alch021') {
                        setAlisonIssues(data.statistics.counts.closed);
                    }
                });
        }

        let emails = new Map();
        emails.set('kylecolburn38@utexas.edu', 'Kyle');
        emails.set('adeng1433@gmail.com', 'Andrew');
        emails.set('alexwu2005@gmail.com', 'Alex');
        emails.set('franketang@utexas.edu', 'Franke');
        emails.set('55422893+FTang21@users.noreply.github.com', 'Franke');
        emails.set('ttrinh1.bisd@gmail.com', 'Trung');
        emails.set('ls@LAPTOP-NF4V8DA1.localdomain', 'Trung');
        emails.set('calison@DESKTOP-9D79SE3.localdomain', 'Alison');
        emails.set('alisoncheung021@gmail.com', 'Alison');

        // Get Total Commits and Member Commits
        for (let pageNum = 1; pageNum < 5; pageNum++){
            axios(`https://gitlab.com/api/v4/projects/21238733/repository/commits?per_page=100&page=${pageNum}`)
                .then(r => {
                    return r.data;
                })
                .then(data => {
                    setTotalCommits((prevState) => prevState += data.length);
                    for (const commitData in data) {
                        let user = emails.get(data[commitData].author_email);
                        if (user === 'Kyle') {
                            setKyleCommits((prevState) => prevState + 1);
                        } else if (user === 'Andrew') {
                            setAndrewCommits((prevState) => prevState + 1);
                        } else if (user === 'Alex') {
                            setAlexCommits((prevState) => prevState + 1);
                        } else if (user === 'Franke') {
                            setFrankeCommits((prevState) => prevState + 1);
                        } else if (user === 'Trung') {
                            setTrungCommits((prevState) => prevState + 1);
                        } else if (user === 'Alison') {
                            setAlisonCommits((prevState) => prevState + 1);
                        }
                    }
                });
        }

    }, []);

    return (
            <Container>
                <br />
                <h2 className="text-center">Overall Data</h2>
                <br />
                <Row>
                    <Col>
                        <p>Total Commits: {totalCommits}</p>
                    </Col>
                    <Col>
                        <p>Total Issues: {totalIssues}</p>
                    </Col>
                    <Col>
                        <p>Total Unit Tests: {totalTests}</p>
                    </Col>
                </Row>
                <br />
                <br />
                <h2 className="text-center">Member Data</h2>
                <br />
                <Row>
                    <Col>
                        <Card className={classes.root}>
                            <CardHeader
                                title={'Kyle Colburn'} titleTypographyProps={{variant:'h5'}} style={{ textAlign: 'center' }}
                            />
                            <CardContent>
                                <img src={kyleImg} className="my_img mx-auto d-block" />
                                <br />
                                <Typography color="textSecondary" component="p">

                                <p>Systems nerd extraordinaire, I also play for the UT Varsity Overwatch team and love sleeping with my cat, Ink. He’s a real homie. I like watching anime and Youtube in my limited free time.</p>
                                <p>De-Facto project lead, project documentation, and Backend/API work.</p>
                                <br/><br/>
                                <p>
                                Number of Issues: {kyleIssues}<br />
                                Number of Commits: {kyleCommits}<br />
                                Number of Tests: {kyleTests}
                                </p>
                                </Typography>
                            </CardContent>
                        </Card>
                    </Col>
                    <Col>
                        <Card className={classes.root}>
                            <CardHeader
                                title={'Andrew Deng'} titleTypographyProps={{variant:'h5'}} style={{ textAlign: 'center' }}
                            />
                            <CardContent>
                                <img src={andrewImg} className="my_img mx-auto d-block" />
                                <br />
                                <Typography color="textSecondary" component="p">

                                <p>3rd year UTCS alongside a Japanese certificate. MCU, Zelda, FF, KH, Smash, Anime, Manga, music, culinary nerd. I’m a lead player in The Pans of Texas, UT’s steel drum band. Nowadays, I spend my nonexistent free time hyping myself up for upcoming game releases.</p>
                                <p>Project meeting pacemaker, backend/API, AWS hosting</p>
                                <p>
                                Number of Issues: {andrewIssues}<br />
                                Number of Commits: {andrewCommits}<br />
                                Number of Tests: {andrewTests}
                                </p>
                                </Typography>
                            </CardContent>
                        </Card>
                    </Col>
                    <Col>
                    <Card className={classes.root}>
                        <CardHeader
                            title={'Alex Wu'} titleTypographyProps={{variant:'h5'}} style={{ textAlign: 'center' }}
                        />
                        <CardContent>
                            <img src={alexImg} className="my_img mx-auto d-block" />
                            <br />
                            <Typography color="textSecondary" component="p">

                            <p>Hello! I'm a third year CS student at UT Austin. Most of my days are spent on my passion video games, developing or playing them. If you ever catch me outside, I'll most likely be at the UT Archery range.</p>
                            <p>Backend/Api, AWS amplify</p>
                            <br/><br/>
                            <p>
                            Number of Issues: {alexIssues}<br />
                            Number of Commits: {alexCommits}<br />
                            Number of Tests: {alexTests}
                            </p>
                            </Typography>
                        </CardContent>
                    </Card>
                    </Col>
                </Row>
                <br />
                <Row>
                    <Col>
                    <Card className={classes.root}>
                        <CardHeader
                            title={'Franke Tang'} titleTypographyProps={{variant:'h5'}} style={{ textAlign: 'center' }}
                        />
                        <CardContent>
                            <img src={frankeImg} className="my_img mx-auto d-block" />
                            <br />
                            <Typography color="textSecondary" component="p">

                            <p>I’m a second year CS student at UT Austin. I love video games and my dog Chewie, a Siberian Husky. Other interests lies in Anime, DnD, Running, Ultimate Frisbee, Cooking, and a slew of different other things.</p>
                            <p>Frontend</p>
                            <p>
                            Number of Issues: {frankeIssues}<br />
                            Number of Commits: {frankeCommits}<br />
                            Number of Tests: {frankeTests}
                            </p>
                            </Typography>
                        </CardContent>
                    </Card>
                    </Col>
                    <Col>
                        <Card className={classes.root}>
                            <CardHeader
                                title={'Trung Trinh'} titleTypographyProps={{variant:'h5'}} style={{ textAlign: 'center' }}
                            />
                            <CardContent>
                                <img src={trungImg} className="my_img mx-auto d-block" />
                                <br />
                                <Typography color="textSecondary" component="p">

                                <p>4th year Computer Science UT student. I enjoy video gaming and watching horror movies. Other interests include anime, cooking, bouldering, and Brazilian Jiu-jitsu (I’m not that good at the physical activities, but I enjoy them).</p>
                                <p>Frontend</p>
                                <p>
                                Number of Issues: {trungIssues}<br />
                                Number of Commits: {trungCommits}<br />
                                Number of Tests: {trungTests}
                                </p>
                                </Typography>
                            </CardContent>
                        </Card>
                    </Col>
                    <Col>
                    <Card className={classes.root}>
                        <CardHeader
                            title={'Alison Cheung'} titleTypographyProps={{variant:'h5'}} style={{ textAlign: 'center' }}
                        />
                        <CardContent>
                            <img src={alisonImg} className="my_img mx-auto d-block" />
                            <br />
                            <Typography color="textSecondary" component="p">

                            <p>I’m a fourth-year Computer Science student at UT. I take way too many naps and watch too much anime. Currently, playing too many rhythm games.</p>
                            <p>Frontend</p>
                            <br /><br />
                            <p>
                            Number of Issues: {alisonIssues}<br />
                            Number of Commits: {alisonCommits}<br />
                            Number of Tests: {alisonTests}
                            </p>
                            </Typography>
                        </CardContent>
                    </Card>
                    </Col>
                </Row>
            </Container>
    )
}

export default MemberInfo
