import requests
import json
from sqlalchemy.orm import sessionmaker, load_only
from sqlalchemy import *
from tables import *

# Create the tables of data that we'll be using to put the media for each vehicle into the database
#
# Media and mappings:                                                                   Table Required?
#   Energy Source:  
#       Map energy source       -> Url of a photo of the energy source                  Y 
#       Map energy source       -> Link to the EIA page for that energy source          Y
#
#   Vehicle: 
#       Map vehicle             -> Url of a photo of that vehicle                       N (Query the vehicles in the DB and store/set these programatically)
#       Map vehicle             -> Twitter feed of that manufacturer                    N (Front-end doing this)
#
#   Infrastructure:
#       Map infrastructure      -> Google maps embedded map of the address              N (Front-end doing this)
#       Map infrastructure      -> ???

# Set up db session
print('set up db session')
engine = create_engine(getURI())
Session = sessionmaker(bind=engine)

s = Session()

# # Tables Start
# print('tables start')
# es_to_photo_url = {
#     'Wood and wood derived fuels': "https://images.pexels.com/photos/1435511/pexels-photo-1435511.jpeg", 
#     'Wind': "https://images.pexels.com/photos/414837/pexels-photo-414837.jpeg", 
#     'Solar': "https://images.pexels.com/photos/159397/solar-panel-array-power-sun-electricity-159397.jpeg", 
#     'Petroleum Coke': "https://upload.wikimedia.org/wikipedia/commons/thumb/8/86/Petrolkoks_IMG_6166.jpg/1200px-Petrolkoks_IMG_6166.jpg", 
#     'Nuclear': "https://images.pexels.com/photos/459728/pexels-photo-459728.jpeg", 
#     'Natural Gas': "https://images.pexels.com/photos/266896/pexels-photo-266896.jpeg", 
#     'Liquid Petroleum': "https://images.pexels.com/photos/162568/oil-pump-jack-sunset-clouds-silhouette-162568.jpeg", 
#     'Hydroelectric': "https://images.pexels.com/photos/2796964/pexels-photo-2796964.jpeg", 
#     'Geothermal': "https://upload.wikimedia.org/wikipedia/commons/thumb/3/3b/Krafla_geothermal_power_station_wiki.jpg/1200px-Krafla_geothermal_power_station_wiki.jpg", 
#     'Coal': "https://images.pexels.com/photos/46801/coal-briquette-black-46801.jpeg", 
#     'Biomass': "https://next3-assets.s3.amazonaws.com/journeys/241/description_backgrounds-1569283393-biofuel_intro_72dpi.png",
# }

# es_to_web_link = {
#     'Wood and wood derived fuels': "https://en.wikipedia.org/wiki/Wood_fuel", 
#     'Wind': "https://www.eia.gov/energyexplained/wind/", 
#     'Solar': "https://www.eia.gov/energyexplained/solar/", 
#     'Petroleum Coke': "https://www.eia.gov/energyexplained/oil-and-petroleum-products/", 
#     'Nuclear': "https://www.eia.gov/energyexplained/nuclear/", 
#     'Natural Gas': "https://www.eia.gov/energyexplained/natural-gas/", 
#     'Liquid Petroleum': "https://www.eia.gov/energyexplained/oil-and-petroleum-products/", 
#     'Hydroelectric': "https://www.eia.gov/energyexplained/hydropower/", 
#     'Geothermal': "https://www.eia.gov/energyexplained/geothermal/", 
#     'Coal': "https://www.eia.gov/energyexplained/coal/", 
#     'Biomass': "https://www.eia.gov/energyexplained/biofuels/",
# }

# print('Load ES Table data into the database columns')
# ### Load ES Table data into the database columns
# # Energy source data insertion

# for name in es_to_photo_url:
#     s.query(EnergySource).filter(EnergySource.name == name).update({EnergySource.image_url: es_to_photo_url[name], EnergySource.information_url: es_to_web_link[name]}, synchronize_session = False)
# s.commit()
# s.close()


print('### Get images for all vehicles and store them in the database ')
### Get images for all vehicles and store them in the database 
# Get all the vehicles in the DB
vehicle_list = []
# vehicle_list = [s.query(Vehicles).get(12434)]
vehicle_list = [r for r in s.query(Vehicles).distinct()]

print(len(vehicle_list))

exit()

# print('# For each vehicle, search an image for it, and store the url in the url column')
# # For each vehicle, search an image for it, and store the url in the url column
# for v in vehicle_list:
#     img_url_req = requests.get("https://serpapi.com/search.json?engine=google&q="+v.get_dict()['name']+"&google_domain=google.com&ijn=0&tbm=isch&api_key=0420d2c23e29be6bbae6beb42694e3bd993ac980ad9200f0cda7ef10c5015cb7")
#     img_url_json = img_url_req.json()['images_results']
#     img_url_str = img_url_json[0]['original']
#     s.query(Vehicles).filter(Vehicles.id == v.get_dict()['id']).update({Vehicles.vehicle_image_url: img_url_str}, synchronize_session = False)

# print('# finish and close the session')
# finish and close the session
s.commit()
s.close()