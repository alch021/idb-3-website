#makes get requests, cleans data, and posts to the econyoom database
import requests
# import tables
import json
from sqlalchemy import Column, Integer, Boolean, String, Date, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from tables import EnergySource, getURI

### Energy Sources

econyoom_energy_source_list = []



## Coal
coal = {}
totalpower_coal_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.COW-US-98.A")
totalpower_coal_json = totalpower_coal_req.json()['series'][0]['data']
elecutil_coal_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.COW-US-1.A")
elecutil_coal_json = elecutil_coal_req.json()['series'][0]['data']
indprod_coal_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.COW-US-94.A")
indprod_coal_json = indprod_coal_req.json()['series'][0]['data']
allcom_coal_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.COW-US-96.A")
allcom_coal_json = allcom_coal_req.json()['series'][0]['data']
totalco2_coal_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=EMISS.CO2-TOTV-TT-CO-US.A")
totalco2_coal_json = totalco2_coal_req.json()['series'][0]['data']


coal_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=BIOD&recent=false&expired=false&local=false")
coal_laws_json = coal_laws_req.json()['result']

coal['name'] = "Coal"
coal['supported_vehicles'] = []
coal['supported_charging_stations'] = []
coal['renewability'] = True

coal['total_power'] = totalpower_coal_json[0][1]
coal['electric_utility'] = elecutil_coal_json[0][1]
coal['independent_producers'] = indprod_coal_json[0][1]
coal['all_commercial'] = allcom_coal_json[0][1]
coal['co2_emissions'] = totalco2_coal_json[0][1]

coal['output_energy_type'] = "Heat into electricity"

coal_laws = ""
for d in coal_laws_json:
    coal_laws += d['title']
    coal_laws += ", "

coal['laws_incentives'] = coal_laws.rstrip(", ")
coal['description'] = "Coal is a combustible black or brownish-black sedimentary rock with a high amount of carbon and hydrocarbons. Coal is classified as a nonrenewable energy source because it takes millions of years to form. Coal contains the energy stored by plants that lived hundreds of millions of years ago in swampy forests."
#https://www.eia.gov/energyexplained/coal/#:~:text=Coal%20is%20a%20combustible%20black,years%20ago%20in%20swampy%20forests.
econyoom_energy_source_list.append(coal)

## Natural Gas
cng = {}
totalpower_cng_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NG-US-98.A")
totalpower_cng_json = totalpower_cng_req.json()['series'][0]['data']
elecutil_cng_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NG-US-1.A")
elecutil_cng_json = elecutil_cng_req.json()['series'][0]['data']
indprod_cng_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NG-US-94.A")
indprod_cng_json = indprod_cng_req.json()['series'][0]['data']
allcom_cng_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NG-US-96.A")
allcom_cng_json = allcom_cng_req.json()['series'][0]['data']
totalco2_cng_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=EMISS.CO2-TOTV-TT-NG-US.A")
totalco2_cng_json = totalco2_cng_req.json()['series'][0]['data']

cng_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=NG&recent=false&expired=false&local=false")
cng_laws_json = cng_laws_req.json()['result']

cng['name'] = "Natural Gas"
cng['supported_vehicles'] = []
cng['supported_charging_stations'] = []
cng['renewability'] = False

cng['total_power'] = totalpower_cng_json[0][1]
cng['electric_utility'] = elecutil_cng_json[0][1]
cng['independent_producers'] = indprod_cng_json[0][1]
cng['all_commercial'] = allcom_cng_json[0][1]

cng['co2_emissions'] = totalco2_cng_json[0][1]

cng['output_energy_type'] = "Directly as fuel, or heat into electricity"

cng_laws = ""
for d in cng_laws_json:
    cng_laws += d['title']
    cng_laws += ", "

cng['laws_incentives'] = cng_laws.rstrip(", ")
cng['description'] = "Natural gas is a fossil energy source that formed deep beneath the earth's surface. Natural gas contains many different compounds. The largest component of natural gas is methane, a compound with one carbon atom and four hydrogen atoms (CH4). Natural gas also contains smaller amounts of natural gas liquids (NGL, which are also hydrocarbon gas liquids), and nonhydrocarbon gases, such as carbon dioxide and water vapor. We use natural gas as a fuel and to make materials and chemicals."
#https://www.eia.gov/energyexplained/natural-gas/

econyoom_energy_source_list.append(cng)

## Petroleum Liquids
pet = {}
totalpower_pet_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PEL-US-98.A")
totalpower_pet_json = totalpower_cng_req.json()['series'][0]['data']
elecutil_pet_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PEL-US-1.A")
elecutil_pet_json = elecutil_pet_req.json()['series'][0]['data']
indprod_pet_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PEL-US-94.A")
indprod_pet_json = indprod_pet_req.json()['series'][0]['data']
allcom_pet_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PEL-US-96.A")
allcom_pet_json = allcom_pet_req.json()['series'][0]['data']
totalco2_pet_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=EMISS.CO2-TOTV-TT-PE-US.A")
totalco2_pet_json = totalco2_pet_req.json()['series'][0]['data']

pet_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=BIOD&recent=false&expired=false&local=false")
pet_laws_json = pet_laws_req.json()['result']

pet['name'] = "Liquid Petroleum"
pet['supported_vehicles'] = []
pet['supported_charging_stations'] = []
pet['renewability'] = False

pet['total_power'] = totalpower_pet_json[0][1]
pet['electric_utility'] = elecutil_pet_json[0][1]
pet['independent_producers'] = indprod_pet_json[0][1]
pet['all_commercial'] = allcom_pet_json[0][1]

pet['co2_emissions'] = totalco2_pet_json[0][1]

pet['output_energy_type'] = "Processed into fuels or used to generate electricity."

pet_laws = ""
for d in pet_laws_json:
    pet_laws += d['title']
    pet_laws += ", "

pet['laws_incentives'] = pet_laws.rstrip(", ")
pet['description'] = "We call crude oil and petroleum fossil fuels because they are mixtures of hydrocarbons that formed from the remains of animals and plants (diatoms) that lived millions of years ago in a marine environment before the existence of dinosaurs. Over millions of years, the remains of these animals and plants were covered by layers of sand, silt, and rock. Heat and pressure from these layers turned the remains into what we now call crude oil or petroleum. The word petroleum means rock oil or oil from the earth."
#https://www.eia.gov/energyexplained/oil-and-petroleum-products/


econyoom_energy_source_list.append(pet)

## Petroleum Coke
pec = {}
totalpower_pec_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PC-US-99.A")
totalpower_pec_json = totalpower_pec_req.json()['series'][0]['data']
elecutil_pec_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PC-US-1.A")
elecutil_pec_json = elecutil_pec_req.json()['series'][0]['data']
indprod_pec_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PC-US-94.A")
indprod_pec_json = indprod_pec_req.json()['series'][0]['data']
allcom_pec_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.PC-US-96.A")
allcom_pec_json = allcom_pec_req.json()['series'][0]['data']
totalco2_pec_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=EMISS.CO2-TOTV-TT-PE-US.A")
totalco2_pec_json = totalco2_pec_req.json()['series'][0]['data']


pec['name'] = "Petroleum Coke"
pec['supported_vehicles'] = []
pec['supported_charging_stations'] = []
pec['renewability'] = False

pec['total_power'] = totalpower_pec_json[0][1]
pec['electric_utility'] = elecutil_pec_json[0][1]
pec['independent_producers'] = indprod_pec_json[0][1]
pec['all_commercial'] = allcom_pec_json[0][1]

pec['co2_emissions'] = totalco2_pec_json[0][1]

pec['output_energy_type'] = "Heat into electricity"

pec['laws_incentives'] = "N/A"
pec['description'] = "Petroleum coke, abbreviated coke or petcoke, is a final carbon-rich solid material that derives from oil refining, and is one type of the group of fuels referred to as cokes. Petcoke is the coke that, in particular, derives from a final cracking process—a thermo-based chemical engineering process that splits long chain hydrocarbons of petroleum into shorter chains—that takes place in units termed coker units. (Other types of coke are derived from coal.) Stated succinctly, coke is the carbonization product of high-boiling hydrocarbon fractions obtained in petroleum processing (heavy residues). Petcoke is also produced in the production of synthetic crude oil (syncrude) from bitumen extracted from Canada’s oil sands and from Venezuela's Orinoco oil sands."
#https://en.wikipedia.org/wiki/Petroleum_coke#:~:text=Stated%20succinctly%2C%20coke%20is%20the,from%20Venezuela's%20Orinoco%20oil%20sands.

econyoom_energy_source_list.append(pec)

## Nuclear
nuc = {}
totalpower_nuc_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NUC-US-98.A")
totalpower_nuc_json = totalpower_pec_req.json()['series'][0]['data']
elecutil_nuc_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NUC-US-1.A")
elecutil_nuc_json = elecutil_nuc_req.json()['series'][0]['data']
indprod_pet_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.NUC-US-94.A")
indprod_nuc_json = indprod_pet_req.json()['series'][0]['data']
# Data unavailable: allcom_pet_req 

nuc_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=ELEC&recent=false&expired=false&local=false")
nuc_laws_json = nuc_laws_req.json()['result']

nuc['name'] = "Nuclear"
nuc['supported_vehicles'] = []
nuc['supported_charging_stations'] = []
nuc['renewability'] = True

nuc['total_power'] = totalpower_nuc_json[0][1]
nuc['electric_utility'] = elecutil_nuc_json[0][1]
nuc['independent_producers'] = indprod_nuc_json[0][1]
nuc['all_commercial'] = -1

nuc['co2_emissions'] = -1

nuc['output_energy_type'] = "Fusion reaction generates heat into electricity"

nuc_laws = ""
for d in nuc_laws_json:
    nuc_laws += d['title']
    nuc_laws += ", "

nuc['laws_incentives'] = nuc_laws.rstrip(", ")
nuc['description'] = "Nuclear power is the use of nuclear reactions that release nuclear energy to generate heat, which most frequently is then used in steam turbines to produce electricity in a nuclear power plant. Nuclear power can be obtained from nuclear fission, nuclear decay and nuclear fusion reactions."
econyoom_energy_source_list.append(nuc)

## Hydroelectric
hyc = {}
totalpower_hyc_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.HYC-US-98.A")
totalpower_hyc_json = totalpower_pec_req.json()['series'][0]['data']
elecutil_hyc_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.HYC-US-1.A")
elecutil_hyc_json = elecutil_hyc_req.json()['series'][0]['data']
indprod_hyc_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.HYC-US-94.A")
indprod_hyc_json = indprod_hyc_req.json()['series'][0]['data']
allcom_hyc_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.HYC-US-96.A")
allcom_hyc_json = allcom_hyc_req.json()['series'][0]['data']

hyc_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=ELEC&recent=false&expired=false&local=false")
hyc_laws_json = hyc_laws_req.json()['result']

hyc['name'] = "Hydroelectric"
hyc['supported_vehicles'] = []
hyc['supported_charging_stations'] = []
hyc['renewability'] = True

hyc['total_power'] = totalpower_hyc_json[0][1]
hyc['electric_utility'] = elecutil_hyc_json[0][1]
hyc['independent_producers'] = indprod_hyc_json[0][1]
hyc['all_commercial'] = allcom_hyc_json[0][1]

hyc['co2_emissions'] = -1

hyc['output_energy_type'] = "Movement of water into electricity"

hyc_laws = ""
for d in hyc_laws_json:
    hyc_laws += d['title']
    hyc_laws += ", "

hyc['laws_incentives'] = hyc_laws.rstrip(", ")

#https://www.britannica.com/science/hydroelectric-power
hyc['description'] = "Hydroelectric power, also called hydropower, electricity produced from generators driven by turbines that convert the potential energy of falling or fast-flowing water into mechanical energy."

econyoom_energy_source_list.append(hyc)

## Wind
wnd = {}
totalpower_wnd_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WND-US-98.A")
totalpower_wnd_json = totalpower_wnd_req.json()['series'][0]['data']
elecutil_wnd_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WND-US-1.A")
elecutil_wnd_json = elecutil_wnd_req.json()['series'][0]['data']
indprod_wnd_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WND-US-94.A")
indprod_wnd_json = indprod_wnd_req.json()['series'][0]['data']
allcom_wnd_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WND-US-96.A")
allcom_wnd_json = allcom_wnd_req.json()['series'][0]['data']

wnd_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=ELEC&recent=false&expired=false&local=false")
wnd_laws_json = wnd_laws_req.json()['result']

wnd['name'] = "Wind"
wnd['supported_vehicles'] = []
wnd['supported_charging_stations'] = []
wnd['renewability'] = True

wnd['total_power'] = totalpower_wnd_json[0][1]
wnd['electric_utility'] = elecutil_wnd_json[0][1]
wnd['independent_producers'] = indprod_wnd_json[0][1]
wnd['all_commercial'] = allcom_wnd_json[0][1]

wnd['co2_emissions'] = -1

wnd['output_energy_type'] = "Movement of air into electricity"

wnd_laws = ""
for d in wnd_laws_json:
    wnd_laws += d['title']
    wnd_laws += ", "

wnd['laws_incentives'] = wnd_laws.rstrip(", ")

# https://openei.org/wiki/Wind_energy
wnd['description'] = "Wind energy (or wind power) describes the process by which wind is used to generate electricity. Wind turbines convert the kinetic energy in the wind into mechanical power. A generator can convert mechanical power into electricity[2]. Mechanical power can also be utilized directly for specific tasks such as pumping water."

econyoom_energy_source_list.append(wnd)

## Geothermal
geo = {}
totalpower_geo_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.GEO-US-98.A")
totalpower_geo_json = totalpower_geo_req.json()['series'][0]['data']
elecutil_geo_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.GEO-US-1.A")
elecutil_geo_json = elecutil_geo_req.json()['series'][0]['data']
indprod_geo_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.GEO-US-94.A")
indprod_geo_json = indprod_geo_req.json()['series'][0]['data']
allcom_geo_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.GEO-US-96.A")
allcom_geo_json = allcom_geo_req.json()['series'][0]['data']

geo_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=ELEC&recent=false&expired=false&local=false")
geo_laws_json = geo_laws_req.json()['result']

geo['name'] = "Geothermal"
geo['supported_vehicles'] = []
geo['supported_charging_stations'] = []
geo['renewability'] = True

geo['total_power'] = totalpower_geo_json[0][1]
geo['electric_utility'] = elecutil_geo_json[0][1]
geo['independent_producers'] = indprod_geo_json[0][1]
geo['all_commercial'] = allcom_geo_json[0][1]

geo['co2_emissions'] = -1

geo['output_energy_type'] = "Heat from the Earth into electricity"
geo_laws = ""
for d in geo_laws_json:
    geo_laws += d['title']
    geo_laws += ", "

geo['laws_incentives'] = geo_laws.rstrip(", ")
#https://en.wikipedia.org/wiki/Geothermal_energy
geo['description'] = "Geothermal energy is the thermal energy generated and stored in the Earth. Thermal energy is the energy that determines the temperature of matter."

econyoom_energy_source_list.append(geo)

## Wood and wood-derived Fuels
wod = {}
totalpower_wod_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WWW-US-98.A")
totalpower_wod_json = totalpower_wod_req.json()['series'][0]['data']
elecutil_wod_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WWW-US-1.A")
elecutil_wod_json = elecutil_wod_req.json()['series'][0]['data']
indprod_wod_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WWW-US-94.A")
indprod_wod_json = indprod_wod_req.json()['series'][0]['data']
allcom_wod_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WWW-US-96.A")
allcom_wod_json = allcom_wod_req.json()['series'][0]['data']

wod_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=ELEC&recent=false&expired=false&local=false")
wod_laws_json = wod_laws_req.json()['result']

wod['name'] = "Wood and wood derived fuels"
wod['supported_vehicles'] = []
wod['supported_charging_stations'] = []
wod['renewability'] = True

wod['total_power'] = totalpower_wod_json[0][1]
wod['electric_utility'] = elecutil_wod_json[0][1]
wod['independent_producers'] = indprod_wod_json[0][1]
wod['all_commercial'] = allcom_wod_json[0][1]

wod['co2_emissions'] = -1

wod['output_energy_type'] = "Heat from burning into electricity"
wod_laws = ""
for d in wod_laws_json:
    wod_laws += d['title']
    wod_laws += ", "

wod['laws_incentives'] = wod_laws.rstrip(", ")
#https://en.wikipedia.org/wiki/Wood_fuel
wod['description'] = "Wood fuel (or fuelwood) is a fuel such as firewood, charcoal, chips, sheets, pellets, and sawdust."

econyoom_energy_source_list.append(wod)

## Other Biomass
wst = {}
totalpower_was_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WAS-US-98.A")
totalpower_was_json = totalpower_was_req.json()['series'][0]['data']
elecutil_was_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WAS-US-1.A")
elecutil_was_json = elecutil_was_req.json()['series'][0]['data']
indprod_was_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WAS-US-94.A")
indprod_was_json = indprod_was_req.json()['series'][0]['data']
allcom_was_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.WAS-US-96.A")
allcom_was_json = allcom_was_req.json()['series'][0]['data']

wst_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=BIOD&recent=false&expired=false&local=false")
wst_laws_json = wst_laws_req.json()['result']

wst['name'] = "Biomass"
wst['supported_vehicles'] = []
wst['supported_charging_stations'] = []
wst['renewability'] = True

wst['total_power'] = totalpower_was_json[0][1]
wst['electric_utility'] = elecutil_was_json[0][1]
wst['independent_producers'] = indprod_was_json[0][1]
wst['all_commercial'] = allcom_was_json[0][1]

wst['co2_emissions'] = -1

wst['output_energy_type'] = "Heat from burning into electricity"

wst_laws = ""
for d in wst_laws_json:
    wst_laws += d['title']
    wst_laws += ", "

wst['laws_incentives'] = wod_laws.rstrip(", ")

#https://en.wikipedia.org/wiki/Biomass
wst['description'] = "Biomass is plant or animal material used for energy production (electricity or heat), or in various industrial processes as raw substance for a range of products. It can be purposely grown energy crops (e.g. miscanthus, switchgrass), wood or forest residues, waste from food crops (wheat straw, bagasse), horticulture (yard waste), food processing (corn cobs), animal farming (manure, rich in nitrogen and phosphorus), or human waste from sewage plants."

econyoom_energy_source_list.append(wst)

## Solar
sun = {}
totalpower_sun_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.SUN-US-98.A")
totalpower_sun_json = totalpower_sun_req.json()['series'][0]['data']
elecutil_sun_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.SUN-US-1.A")
elecutil_sun_json = elecutil_sun_req.json()['series'][0]['data']
indprod_sun_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.SUN-US-94.A")
indprod_sun_json = indprod_sun_req.json()['series'][0]['data']
allcom_sun_req = requests.get("http://api.eia.gov/series/?api_key=34f87b8617879f63842e8c664e64f14d&series_id=ELEC.GEN.SUN-US-96.A")
allcom_sun_json = allcom_sun_req.json()['series'][0]['data']

sun_laws_req = requests.get("https://developer.nrel.gov/api/transportation-incentives-laws/v1.json?api_key=ut7K5JJIJvS1JR0dm5J5dl0Sntu4B1RUDqZEfFp9&technology=ELEC&recent=false&expired=false&local=false")
sun_laws_json = sun_laws_req.json()['result']

sun['name'] = "Solar"
sun['supported_vehicles'] = []
sun['supported_charging_stations'] = []
sun['renewability'] = True

sun['total_power'] = totalpower_sun_json[0][1]
sun['electric_utility'] = elecutil_sun_json[0][1]
sun['independent_producers'] = indprod_sun_json[0][1]
sun['all_commercial'] = allcom_sun_json[0][1]

sun['co2_emissions'] = -1

sun['output_energy_type'] = "Solar energy into electricity"

sun_laws = ""
for d in sun_laws_json:
    sun_laws += d['title']
    sun_laws += ", "

sun['laws_incentives'] = sun_laws.rstrip(", ")

#https://en.wikipedia.org/wiki/Solar_power
sun['description'] = "Solar power is the conversion of energy from sunlight into electricity, either directly using photovoltaics (PV), indirectly using concentrated solar power, or a combination. Concentrated solar power systems use lenses or mirrors and solar tracking systems to focus a large area of sunlight into a small beam."

econyoom_energy_source_list.append(sun)




engine = create_engine(getURI())
Session = sessionmaker(bind=engine)
s = Session()

s.query(EnergySource).delete()
s.commit()
for d in econyoom_energy_source_list:
    print(d)
    esource = EnergySource(
        name = d["name"],
        supported_vehicles = d["supported_vehicles"],
        supported_charging_stations = d["supported_charging_stations"], 
        renewability = d["renewability"], 
        total_power = d["total_power"], 
        electric_utility = d["electric_utility"], 
        independent_producers = d["independent_producers"], 
        all_commercial = d["all_commercial"], 
        output_energy_type = d["output_energy_type"], 
        laws_incentives = d["laws_incentives"], 
        co2_emissions = d["co2_emissions"], 
        description = d["description"]
    )
    s.add(esource)
    s.commit()

s.close()